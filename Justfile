install:
  npm i
  npm i -g pm2 --force

start:
  pm2 start "npm exec decap-server" --name proxy
  pm2 start "npm exec hugo server -- --disableFastRender --environment local --disableLiveReload" --name server
  pm2 start "npm exec pagefind -- --site ./public/ --output-path public/pagefind" --name search
  @echo "Go to http://localhost:1313"

stop:
  pm2 stop proxy
  pm2 stop server
  pm2 stop search

restart: stop start