---
published: 2024-06-04T00:00:00.000Z
title: Nous contacter en quelques clics
meta:
  keywords:
    - contact
    - végétaclick
    - plateforme végétal
    - troc de plantes
    - achat de plantes
    - garde de plantes
    - création de plantes
    - passionnés de végétal
    - service client
    - support végétal
  description: Contactez Végétaclick, la première plateforme dédiée aux passionnés
    du végétal. Pour toute question sur l'achat, le troc, la création ou la
    garde de plantes, notre équipe est à votre écoute.
image: /img/img-cover-contact.webp
slug: nous-contacter
lastmod: 2024-10-03T18:42:24.342Z
allowUnsafe: true
---
## Vous avez une question ou vous souhaitez tout simplement nous dire bonjour ? 

Pas de souci, vous pouvez nous laisser un message au travers du formulaire ci-dessous ! 

Promis, on vous répond super vite :) 

<form action="mailto:vegetaclick+contact@gmail.com" method="post" enctype="text/plain"><p><label for="name">Nom:</label><br><input style="width:300px;" type="text" id="name" name="Nom" required=""><br><br><label for="email">Email:</label><br><input style="width:300px;" type="email" id="email" name="Email" required=""><br><br><label for="subject">Sujet:</label><br><input style="width:300px;" type="text" id="subject" name="Sujet" required=""><br><br><label for="message">Message:</label><br><textarea style="height:400px;width:800px;" row="10" id="message" name="Message" required=""></textarea><br><br><input type="submit"></p></form>
