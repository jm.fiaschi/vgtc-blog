---
published: 2024-06-04T00:00:00.000Z
title: "Qui sommes-nous ? "
meta:
  keywords:
    - plateforme plantes
    - troc plantes
    - passionnés du végétal
    - acheter plantes en ligne
    - garder plantes
    - échange plantes
    - communauté de jardiniers
    - création de jardins
    - services végétaux
    - plantes à troquer
  description: Qui sommes-nous | Végétaclick - La plateforme des passionnés de
    plantes pour troquer, acheter et partager
image: /img/img-cover-qui-sommes-nous.webp
slug: qui-sommes-nous
lastmod: 2024-10-03T18:36:29.349Z
---
## Bienvenue sur Végétaclick, la première plateforme dédiée aux passionnés du végétal !  

Nous croyons que les plantes sont bien plus que de simples éléments décoratifs : elles enrichissent nos vies et renforcent notre lien avec la nature. 

C'est pourquoi nous avons créé un espace unique, conçu pour rassembler tous les amoureux des plantes, qu'ils soient débutants ou experts.

**Notre mission est simple :** vous permettre d'explorer et de partager votre passion pour le végétal de manière collaborative et accessible. 

**Sur** **Vegetaclick.com**, vous pourrez troquer, acheter ou vendre des plantes.

Vous allez pouvoir prochainement bénéficier de vente flash en direct de nos fleuristes partenaires et réserver des ateliers créatifs chez nos créateurs végétaux.

Nous proposerons également des services de garde de plantes, pour que vous puissiez partir en toute tranquillité en confiant vos plantes à des passionnés qui en prendront soin. 

Localisez également tous les événements physique en lien avec le végétal au travers de notre agenda des manifestations (Salons, foires, ventes ect...).

Et devenez un expert du végétal grâce à nos contenus que vous pouvez retrouvez dès à présent sur notre blog.

Plus qu’une simple plateforme, **Vegetaclick.com** est un véritable réseau d’entraide où chacun peut trouver et offrir des plantes, enrichir ses connaissances et partager son amour pour la nature. 

Ensemble, créons une communauté où la verdure et la passion fleurissent à chaque coin de rue et dans chaque foyer.

Vous pouvez aussi nous suivre sur nos réseaux sociaux :

**Facebook :** <https://www.facebook.com/people/Vegetaclick/61559628805252/>

**Instagram :** <https://www.instagram.com/vegetaclick/>

**TikTok :** <https://www.tiktok.com/@vegetaclick>
