---
administrable: true
image: /img/aeonium.webp
city: lyon
event_type: foire-aux-plantes
meta:
  keywords:
  - Orchidays
summary: Cette plante verte est un véritable bijou pour mon intérieur ! Non
  seulement elle ajoute une touche de verdure apaisante à mon espace, mais elle
  respire la vitalité et la santé. Son entretien est facile et elle semble
  prospérer sous mes soins. Chaque fois que je rentre chez moi, je suis
  accueilli par sa présence rafraîchissante qui égaye instantanément mon humeur.
start_date: 2024-01-01
slug: 2024-06-17-16eme-salon-international-de-l-orchidee-orchidays
lastmod: 2024-08-21T10:19:09.321Z
title: 16ème Salon International de l’Orchidée ”Orchidays”
published: 2024-06-17T20:05:00.000Z
---
<section><div id="flyer">

![flyer](/img/09926336f45bf5b66218b5dc63b8a4e8.webp "flyer")

</div>
<div id="details">
<br><br>
<b>Organisateur :</b><br>Rotary Club Marseille

<b>date :</b>\
Vendredi 9 fév. : 14h-18h -\
Samedi 10 &\
Dimanche 11 fév. : 10h-18h

<b>Lieu :</b>\
Salon des expositions de Bouc Bel Air\
13290 Bouc Bel Air

<b>Tarifs :</b>\
Entrée + Tombola = 5€ \
(gratuit pour les - de 15 ans)

<b>Parking :</b>\
Gratuit

<b>Site internet de l’événement :</b>\
<https://orchidays.com/>

<b>Réseaux sociaux de l’événements:</b>

<div class="sharethis-inline-share-buttons"></div>

</div></section>

## 16 éme édition des orchidée Days

Orchidays à Bouc Bel Air : la magie des orchidées pour une bonne cause !

Bouc Bel Air accueille la 16ème édition du Salon International de l’Orchidée ”Orchidays” qui se tiendra à la Salle des terres Blanches du vendredi 9 au dimanche 11 février 2024. Tous les bénéfices seront reversés aux œuvres caritatives du Rotary.

Pour cette nouvelle édition, Orchidays a, de nouveau, fait appel aux meilleurs exposants et producteurs pour vous présenter leurs sélections. Venues du monde entier et principalement d’Italie, Espagne, Allemagne, Belgique et des quatre coins de France, les Orchidées et autres plantes d’exception s’épanouiront à la Salle des Terres Blanches, pour votre plus grand plaisir !
Au programme de ces 3 jours d’exposition : ateliers et conseils de professionnels, rempotage, artisanat (bijoux, parfums, céramiques…).

Venez découvrir les espèces originales et fascinantes aux parfums enivrants, orchidées luxuriantes, mimosa, plantes exotiques, vanille, plantes à bulbes, épiphytes et autres plantes d’exception !

![](/img/484789bd6f6792112b50aadab0e01e0e.webp)

C’est le voyage extraordinaire que vous propose le salon Orchidées de Bouc Bel Air organisé par le Rotary-Aix-Trévaresse-Durance.
Les bénéfices de cette manifestation seront affectés au « Centre Ressource » d’Aix en Pce qui accompagne les malades du cancer et aux œuvres caritatives du Club.
Sur plus de 1400 m2, ce sont près de 30 exposants qui vous proposeront leurs plus beaux plants et spécimens.
Dès le hall d’entrée, Catherine Gérard de l’association ”Arum et libellule” à St Cannat vous accueillera avec ses compositions florales somptueuses et foisonnantes. Passionnée, Catherine a remporté de nombreux concours dans le monde entier ! 

Comme elle nous le dit, ”mon plaisir, c’est d’enseigner et partager les différentes techniques de l’art floral”. Elle tiendra, en compagnie de son équipe des ateliers créatifs (en continu du vendredi au dimanche) ; Pour s’inscrire : www.orchidays.fr 

Vous pourrez repartir avec vos créations, conçues essentiellement à partir d’orchidées.

Vous pourrez également apprécier l’exposition unique des plus beaux spécimens des producteurs présents. Plusieurs espèces rares seront exposées pour le ravissement de nos yeux.

En 2024, nos exposants emblématiques sont à nouveau présents au salon : les Orchidées Michel Vacherot, Ryanne Orchidée, Orientyorchids, Orchidées Frenzel, Orchidee Del Lago Maggiore, Liloo, Boffo Orchidées, Orchidées Jacq…

Ce salon, c’est l’occasion idéale de discuter avec des producteurs passionnés qui vous donneront de bons conseils pour que vos plantes se sentent bien et refleurissent chaque année afin d’embellir et éclairer vos maisons. Sans oublier le matériel nécessaire à la culture des nombreuses variétés.

Un grand parking est à disposition du public et des visiteurs gratuitement.
