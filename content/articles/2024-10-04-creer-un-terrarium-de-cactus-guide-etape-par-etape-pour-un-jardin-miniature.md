---
category: plantes-d-interieur/cactus-et-plantes-grasses/_index
title: "Créer un terrarium de cactus : Guide étape par étape pour un jardin miniature"
meta:
  keywords:
    - Terrarium de cactus
    - Jardin miniature
    - Créer terrarium
    - Cactus pour débutants
    - Entretien cactus
    - Substrat pour cactus
    - Plantes succulentes
    - Terrarium en verre
    - Décoration intérieure
    - Terrarium ouvert
    - Cactus miniature
    - Sélection de cactus
    - Plantes d’intérieur
    - Choisir cactus
    - Cactus facile à entretenir
    - Jardin intérieur
    - Plantes décoratives
    - Terrarium DIY
    - Aménagement terrarium
    - Entretien terrarium
  description: Découvrez comment créer un magnifique terrarium de cactus grâce à
    ce guide étape par étape. Apprenez à choisir les bonnes plantes, préparer le
    substrat, et personnaliser votre terrarium pour un jardin miniature exotique
    et facile à entretenir.
image: /img/img-cover-cactus.webp
summary: >-
  Les terrariums de cactus sont des jardins miniatures fascinants qui apportent
  une touche de nature et d'exotisme à n'importe quel espace intérieur. Ils sont
  non seulement esthétiques mais aussi relativement faciles à entretenir, ce qui
  les rend parfaits pour les amateurs de plantes de tous niveaux. Dans ce guide
  complet, nous allons vous montrer comment créer un terrarium de cactus en
  suivant des étapes simples, du choix des plantes à l'entretien, en passant par
  la sélection des matériaux et du substrat.


  Qu'est-ce qu'un terrarium de cactus ? 


  Un terrarium est un récipient transparent, souvent en verre, qui contient des plantes et parfois des éléments décoratifs. Contrairement aux terrariums fermés qui maintiennent un environnement humide, les terrariums de cactus sont généralement ouverts pour permettre une bonne circulation de l'air et éviter l'accumulation d'humidité. Les cactus, qui sont des plantes succulentes, nécessitent un environnement sec, ce qui en fait des candidats parfaits pour ce type de terrarium.
slug: 2024-10-04-creer-un-terrarium-de-cactus-guide-etape-par-etape-pour-un-jardin-miniature
lastmod: 2024-10-18T11:21:17.138Z
published: 2024-10-04T14:05:00.000Z
administrable: true
url: fiches-conseils/plantes-d-interieur/cactus-et-plantes-grasses/2024-10-04-creer-un-terrarium-de-cactus-guide-etape-par-etape-pour-un-jardin-miniature
kind: page
---
Les terrariums de cactus sont des jardins miniatures fascinants qui apportent une touche de nature et d'exotisme à n'importe quel espace intérieur. Ils sont non seulement esthétiques mais aussi relativement faciles à entretenir, ce qui les rend parfaits pour les amateurs de plantes de tous niveaux. Dans ce guide complet, nous allons vous montrer comment créer un terrarium de cactus en suivant des étapes simples, du choix des plantes à l'entretien, en passant par la sélection des matériaux et du substrat.

1. Qu'est-ce qu'un terrarium de cactus ?

Un terrarium est un récipient transparent, souvent en verre, qui contient des plantes et parfois des éléments décoratifs. Contrairement aux terrariums fermés qui maintiennent un environnement humide, les terrariums de cactus sont généralement ouverts pour permettre une bonne circulation de l'air et éviter l'accumulation d'humidité. Les cactus, qui sont des plantes succulentes, nécessitent un environnement sec, ce qui en fait des candidats parfaits pour ce type de terrarium.

2. Pourquoi choisir un terrarium de cactus ?

Avant de plonger dans les étapes de création d'un terrarium de cactus, voici quelques raisons pour lesquelles vous pourriez choisir de créer un terrarium de cactus :

Facilité d'entretien : Les cactus nécessitent peu d'eau et sont très tolérants à la négligence, ce qui les rend idéaux pour les personnes occupées ou celles qui n'ont pas la main verte.

Esthétique : Un terrarium de cactus peut être un bel ajout à n'importe quel décor intérieur. Avec leurs formes variées et leurs couleurs vibrantes, les cactus peuvent créer un point focal visuel intéressant.

Personnalisation : Vous pouvez personnaliser votre terrarium avec différentes plantes, pierres, sables colorés et autres éléments décoratifs pour créer un jardin miniature unique.

Espace : Les terrariums de cactus sont parfaits pour les petits espaces car ils ne nécessitent pas beaucoup de place.

3. Choisir les bons cactus pour votre terrarium

Le choix des cactus est une étape essentielle dans la création de votre terrarium. Il est important de choisir des plantes qui ont des besoins similaires en termes de lumière et d'eau.

3.1 Types de cactus recommandés pour un terrarium

Voici quelques types de cactus populaires qui conviennent bien à un terrarium :

Mammillaria : Ce genre de cactus est très diversifié, avec de nombreuses espèces qui présentent de petites formes rondes et épineuses. Ils sont parfaits pour les terrariums en raison de leur taille compacte.

Echinopsis : Aussi appelés cactus boule, ils ont une forme sphérique et produisent parfois de belles fleurs colorées. Leur petite taille les rend idéaux pour les terrariums.

Gymnocalycium : Ces cactus sont reconnaissables à leurs formes rondes et leurs épines souvent colorées. Ils peuvent produire des fleurs étonnantes et sont relativement faciles à entretenir.

Rebutia : De petite taille, ces cactus ont une forme arrondie et produisent des fleurs vives. Leur tolérance à diverses conditions de croissance les rend parfaits pour les terrariums.

3.2 Conseils pour choisir les cactus

Taille : Choisissez des cactus de petite taille qui ne pousseront pas trop rapidement. Les cactus à croissance lente sont idéaux pour les terrariums.

Compatibilité : Assurez-vous que les cactus que vous sélectionnez ont des besoins similaires en lumière et en eau. Cela facilitera l'entretien de votre terrarium.

Couleur et texture : Variez les couleurs et les textures des cactus pour créer un terrarium visuellement intéressant. Mélangez des cactus verts, bleutés, et des variétés avec des épines colorées pour plus de contraste.

4. Sélection du récipient pour le terrarium

Le choix du récipient est crucial pour la création d'un terrarium de cactus. Il doit être suffisamment grand pour contenir les plantes et permettre un bon drainage, tout en étant suffisamment petit pour s'adapter à l'espace disponible.

4.1 Types de récipients adaptés

Bocaux en verre : Les bocaux en verre sont populaires pour les terrariums en raison de leur transparence, qui permet de voir les couches de substrat et les racines des plantes. Assurez-vous que le bocal est assez large pour permettre la croissance des cactus.

Aquariums : Les petits aquariums peuvent également être utilisés comme terrariums. Leur forme rectangulaire permet de créer des paysages miniatures complexes.

Bols ouverts : Un bol ouvert est idéal pour un terrarium de cactus, car il permet une bonne circulation de l'air, ce qui est essentiel pour éviter l'accumulation d'humidité.

Récipients décoratifs : Vous pouvez également utiliser des récipients décoratifs tels que des théières en verre, des vases larges ou des coupes en cristal, tant qu'ils sont assez larges et peu profonds.

4.2 Caractéristiques du récipient idéal

Transparence : Choisissez un récipient transparent pour pouvoir observer la croissance des plantes et l'évolution du substrat.

Ouverture : Optez pour un récipient avec une grande ouverture pour faciliter l'accès lors de la plantation et de l'entretien.

Taille : Le récipient doit être suffisamment grand pour contenir les cactus et permettre une certaine croissance. Cependant, il ne doit pas être trop profond, car les cactus ont des racines peu profondes.

5. Préparer le substrat

Le substrat est l'un des éléments les plus importants de votre terrarium de cactus. Il doit être bien drainé pour éviter la pourriture des racines et fournir suffisamment de support pour les plantes.

5.1 Composants du substrat

Un bon substrat pour les cactus dans un terrarium doit contenir plusieurs couches pour assurer un drainage adéquat et éviter l'accumulation d'eau. Voici les composants typiques d'un substrat pour terrarium de cactus :

Gravier ou cailloux : Une couche de gravier ou de petits cailloux au fond du récipient aide à assurer un bon drainage et empêche l'eau de stagner.

Charbon actif : Une fine couche de charbon actif aide à filtrer les toxines et empêche les mauvaises odeurs causées par l'accumulation d'humidité.

Terre pour cactus : Utilisez un mélange de terre spécialement conçu pour les cactus et les succulentes. Ce type de terre est bien drainé et aéré, ce qui est essentiel pour éviter la pourriture des racines.

5.2 Étapes pour préparer le substrat

Ajouter la couche de drainage : Commencez par ajouter une couche de gravier ou de cailloux au fond du récipient. Cette couche doit avoir une épaisseur de 2 à 3 cm.

Ajouter le charbon actif : Saupoudrez une fine couche de charbon actif sur les cailloux pour aider à purifier l'air et l'eau à l'intérieur du terrarium.

Ajouter la terre pour cactus : Remplissez le récipient avec de la terre pour cactus jusqu'à ce qu'il soit environ aux deux tiers plein. Vous pouvez ajouter plus de terre si nécessaire, en fonction de la taille des cactus.

6. Planter les cactus dans le terrarium

Une fois que votre récipient et votre substrat sont prêts, il est temps de planter vos cactus. Suivez ces étapes pour vous assurer que vos plantes sont bien positionnées et qu'elles ont un bon départ.

6.1 Étapes de plantation

Planifiez la disposition : Avant de planter, planifiez la disposition de vos cactus en les disposant à la surface du substrat. Essayez différentes combinaisons pour trouver la disposition qui vous plaît le plus.

Creusez des trous pour les plantes : À l'aide d'une cuillère ou d'un petit outil de jardinage, creusez des trous dans le substrat pour chaque cactus. Les trous doivent être suffisamment profonds pour accueillir les racines des plantes.

Plantez les cactus : Retirez délicatement les cactus de leurs pots d'origine et placez-les dans les trous que vous avez creusés. Manipulez les cactus avec précaution pour éviter de vous piquer avec les épines. Vous pouvez utiliser une pince ou des gants de jardinage épais pour protéger vos mains.

Compactez légèrement le sol : Une fois les cactus en place, utilisez vos doigts ou un outil de jardinage pour compacter légèrement la terre autour des racines. Cela aidera à stabiliser les plantes et à éliminer les poches d'air.

6.2 Ajout d'éléments décoratifs

Après avoir planté vos cactus, vous pouvez ajouter des éléments décoratifs pour personnaliser votre terrarium. Voici quelques idées :

Roches décoratives : Ajoutez des roches décoratives ou des cailloux colorés pour créer des chemins ou des zones distinctes dans votre terrarium.

Sable coloré : Utilisez du sable coloré pour créer des couches ou des motifs intéressants à la surface du substrat.

Figurines miniatures : Ajoutez des figurines miniatures, telles que des animaux ou des personnages, pour donner vie à votre terrarium.

Coquillages : Les coquillages peuvent ajouter une touche côtière et contrastent bien avec les textures des cactus.

7. Entretien du terrarium de cactus

Bien que les terrariums de cactus soient relativement faciles à entretenir, il est important de suivre quelques règles de base pour assurer la santé et la longévité de vos plantes.

7.1 Arrosage

Les cactus sont des plantes qui stockent l'eau dans leurs tiges et feuilles, ce qui signifie qu'ils n'ont pas besoin d'arrosages fréquents.

Fréquence d'arrosage : Arrosez vos cactus environ une fois par mois ou lorsque le substrat est complètement sec. Il est préférable d'arroser trop peu que trop, car les cactus sont sensibles à l'excès d'eau.

Méthode d'arrosage : Utilisez une seringue, un compte-gouttes ou un arrosoir avec un bec fin pour arroser directement à la base des plantes. Évitez d'arroser les parties aériennes des cactus pour prévenir la pourriture.

7.2 Lumière

Les cactus ont besoin de beaucoup de lumière pour prospérer, mais la quantité de lumière dépend du type de cactus que vous avez choisi.

Lumière directe : Placez votre terrarium dans un endroit où il recevra au moins 6 heures de lumière directe du soleil par jour. Une fenêtre orientée au sud est idéale.

Lumière indirecte : Si vos cactus sont exposés à une lumière intense et directe pendant une longue période, ils peuvent se déshydrater ou brûler. Dans ce cas, placez le terrarium dans un endroit où il recevra une lumière indirecte vive.

7.3 Température et Humidité

Les cactus préfèrent des températures chaudes et une faible humidité, car ils sont originaires de régions arides.

Température idéale : Maintenez une température ambiante entre 18 et 26 °C. Évitez les courants d'air froid et les changements brusques de température.

Humidité : Les cactus n'aiment pas l'humidité élevée. Si vous vivez dans un climat humide, assurez-vous que le terrarium est bien ventilé pour éviter l'accumulation d'humidité.

7.4 Taille et Rempotage

Avec le temps, vos cactus peuvent croître et nécessiter une taille ou un rempotage.

Taille : Retirez les parties mortes ou malades des cactus à l'aide de ciseaux stérilisés. Cela aidera à maintenir la santé des plantes et à prévenir les maladies.

Rempotage : Si les cactus deviennent trop grands pour le terrarium, vous devrez peut-être les rempoter dans un récipient plus grand. Procédez avec précaution pour éviter de casser les racines ou de blesser les cactus.

8. Résolution des problèmes courants

Même avec les meilleurs soins, des problèmes peuvent survenir. Voici quelques problèmes courants que vous pourriez rencontrer avec votre terrarium de cactus et comment les résoudre.

8.1 Pourriture des racines

La pourriture des racines est souvent causée par un excès d'eau ou un mauvais drainage.

Symptômes : Les symptômes de la pourriture des racines incluent des tiges molles, des feuilles tombantes et une décoloration. Les racines peuvent également devenir noires et détrempées.

Solution : Retirez délicatement le cactus affecté du terrarium et coupez les parties pourries des racines. Rempotez dans un substrat sec et bien drainé. Réduisez la fréquence d'arrosage pour prévenir de futures occurrences.

8.2 Croissance lente ou stagnante

Si vos cactus ne semblent pas pousser ou s'ils stagnent, cela peut être dû à un manque de lumière, de nutriments ou à des températures trop basses.

Symptômes : Les cactus peuvent paraître maigres, étiolés, ou leurs feuilles peuvent se plier.

Solution : Assurez-vous que vos cactus reçoivent suffisamment de lumière et ajustez leur emplacement si nécessaire. Vérifiez la température ambiante et assurez-vous qu'elle est adéquate pour la croissance des cactus.

8.3 Étiolation

L'étiolation se produit lorsque les cactus ne reçoivent pas assez de lumière, ce qui les fait s'étirer en longueur à la recherche de lumière.

Symptômes : Les cactus apparaissent allongés, avec des tiges minces et allongées.

Solution : Placez le terrarium dans un endroit où il recevra plus de lumière directe. Coupez les parties étiolées pour encourager une croissance plus compacte et saine.

9. Idées créatives pour personnaliser votre terrarium de cactus

La création d'un terrarium de cactus vous permet de laisser libre cours à votre créativité. Voici quelques idées pour personnaliser votre terrarium :

Thème désertique : Ajoutez du sable, des rochers et des morceaux de bois flotté pour créer un paysage désertique miniature.

Terrarium coloré : Utilisez du sable coloré et des cailloux de différentes teintes pour ajouter de la couleur à votre terrarium.

Miniatures : Ajoutez des figurines miniatures, comme des animaux ou des petites maisons, pour créer un terrarium de cactus fantaisiste.

Couches de substrat : Créez des couches de substrat visible en utilisant du sable de différentes couleurs pour un effet visuel attrayant.

Conclusion

Créer un terrarium de cactus est une activité amusante et gratifiante qui vous permet d'apporter un peu de nature dans votre espace intérieur. En suivant ce guide étape par étape, vous pouvez facilement créer un jardin miniature de cactus qui est non seulement beau mais aussi facile à entretenir. Que vous soyez un débutant ou un amateur de plantes chevronné, un terrarium de cactus est un excellent moyen d'ajouter une touche de verdure à votre maison tout en exprimant votre créativité. Prenez plaisir à choisir vos cactus, à préparer votre récipient et à voir votre petit jardin prospérer au fil du temps.
