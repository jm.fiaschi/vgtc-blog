---
category: graines-de-fleurs-et-bulbes/graines-aromatiques/_index
title: "Les meilleures graines aromatiques à semer en pot : Comment cultiver des
  herbes fraîches sur votre balcon ou terrasse"
meta:
  description: Apprenez à cultiver des herbes aromatiques comme le basilic, la
    menthe ou le persil en pot sur votre balcon ou terrasse. Découvrez les
    meilleures graines, astuces de semis et conseils d’entretien pour une
    récolte fraîche et savoureuse toute l’année.
  keywords:
    - herbes aromatiques en pot
    - graines aromatiques
    - basilic en pot
    - menthe en pot
    - persil en pot
    - culture balcon
    - jardin aromatique
    - semis herbes
    - entretien herbes en pot
    - récolte d’herbes fraîches
    - conseils jardinage terrasse
    - plantation aromatiques
    - herbes pour cuisine
    - graines de coriandre
    - jardiner en pot.
image: /img/img-cover-meilleures-graines-aromatiques-semer-pot.png
summary: Cultiver des herbes aromatiques à partir de graines est une solution
  idéale pour profiter de plantes fraîches et savoureuses sur votre balcon ou
  terrasse, même si vous ne disposez pas d’un grand espace. En plus d’ajouter
  une touche verte à votre environnement, les herbes comme le basilic, la menthe
  ou le persil sont faciles à entretenir et apportent des saveurs et des arômes
  incomparables à vos plats. Dans cet article, nous vous présentons un guide
  détaillé pour choisir et planter des graines aromatiques en pot, ainsi que des
  astuces pour réussir vos semis et entretenir vos plantes pour une récolte
  abondante et durable.
weight: 0
slug: les-meilleures-graines-aromatiques-semer-en-pot-comment-cultiver-des-herbes-fra-ches-sur-votre-balcon-ou-terrasse
lastmod: 2024-12-06T16:42:00.328Z
published: 2024-12-02T18:23:00.000Z
administrable: true
url: fiches-conseils/graines-de-fleurs-et-bulbes/graines-aromatiques/les-meilleures-graines-aromatiques-semer-en-pot-comment-cultiver-des-herbes-fra-ches-sur-votre-balcon-ou-terrasse
kind: page
---
Cultiver des herbes aromatiques à partir de graines est une solution idéale pour profiter de plantes fraîches et savoureuses sur votre balcon ou terrasse, même si vous ne disposez pas d’un grand espace. En plus d’ajouter une touche verte à votre environnement, les herbes comme le basilic, la menthe ou le persil sont faciles à entretenir et apportent des saveurs et des arômes incomparables à vos plats. Dans cet article, nous vous présentons un guide détaillé pour choisir et planter des graines aromatiques en pot, ainsi que des astuces pour réussir vos semis et entretenir vos plantes pour une récolte abondante et durable.

![Pourquoi cultiver des herbes aromatiques en pot ?](/img/img-article-pourquoi-cultiver-herbes-aromatiques-pot.png "Pourquoi cultiver des herbes aromatiques en pot ?")

## 1. Pourquoi cultiver des herbes aromatiques en pot ?

La culture des herbes aromatiques en pot présente de nombreux avantages, particulièrement pour ceux qui vivent en ville ou qui disposent de petits espaces comme un balcon ou une terrasse.

### 1.1 Une récolte toujours à portée de main

Avoir des herbes fraîches à portée de main signifie que vous pouvez cueillir ce dont vous avez besoin en temps réel pour vos recettes. Plus besoin de courir à l’épicerie pour un bouquet de basilic ou de persil !

### 1.2 Facilité d'entretien et contrôle des conditions

Les herbes aromatiques sont généralement faciles à cultiver en pot et demandent peu d’entretien. En pots, vous contrôlez les conditions de culture : le terreau, l’arrosage et même l’exposition au soleil. Cela permet également de déplacer vos pots en fonction des saisons ou des besoins en lumière.

### 1.3 Idéal pour les petits espaces

Même avec un espace réduit, comme un balcon, une terrasse ou un rebord de fenêtre, vous pouvez cultiver une grande variété d’herbes aromatiques. Les pots ou jardinières permettent d’optimiser l’espace tout en apportant une touche décorative et utile.

![Quelles herbes aromatiques choisir pour la culture en pot ?](/img/img-article-quelles-herbes-aromatiques-choisir-pour-culture-pot.png "Quelles herbes aromatiques choisir pour la culture en pot ?")

## 2. Quelles herbes aromatiques choisir pour la culture en pot ?

Certaines herbes aromatiques se prêtent mieux que d’autres à la culture en pot. Voici une sélection des meilleures variétés que vous pouvez cultiver facilement sur votre balcon ou terrasse.

### 2.1 Basilic (Ocimum basilicum)

Le basilic est l'une des herbes aromatiques les plus populaires, notamment en cuisine méditerranéenne. Il pousse bien en pot et nécessite un ensoleillement abondant, mais doit être protégé des courants d’air et du froid.

* **Exposition :** Plein soleil (au moins 6 heures par jour).
* **Arrosage :** Arrosage régulier mais sans excès. Le sol doit rester légèrement humide.
* **Conseils de culture :** Semez les graines au printemps, après les dernières gelées, à environ 1 cm de profondeur. Gardez une distance de 10 cm entre les plants pour favoriser leur développement.

#### Variétés recommandées :

* **Basilic ‘Genovese’ :** La variété classique, idéale pour les sauces et les pestos.
* **Basilic pourpre :** Apporte une couleur et une saveur unique à vos plats.

### 2.2 Menthe (Mentha spp.)

La menthe est une plante très facile à cultiver et parfaite pour les cocktails, les infusions et les desserts. Elle pousse rapidement et peut devenir envahissante, ce qui en fait une excellente candidate pour la culture en pot afin de mieux contrôler son développement.

* **Exposition :** Mi-ombre à plein soleil.
* **Arrosage :** Arrosage fréquent pour maintenir le sol humide.
* **Conseils de culture :** Semez les graines de menthe à la surface du sol et recouvrez-les légèrement. Espacez les plants d’environ 15 cm pour leur permettre de bien s’étendre.

#### Variétés recommandées :

* **Menthe poivrée :** Très parfumée, idéale pour les thés et infusions.
* **Menthe verte :** La variété classique, parfaite pour la cuisine et les cocktails.

### 2.3 Persil (Petroselinum crispum)

Le persil est une autre herbe incontournable en cuisine. Il se cultive bien en pot et préfère une exposition à mi-ombre, surtout lors des fortes chaleurs estivales.

* **Exposition :** Mi-ombre à soleil léger.
* **Arrosage :** Maintenir le sol humide mais bien drainé.
* **Conseils de culture :** Semez les graines de persil directement en pot à environ 1 cm de profondeur. Espacez les plants de 10 à 15 cm pour favoriser leur croissance.

#### Variétés recommandées :

* **Persil plat (italien) :** Plus parfumé et utilisé pour assaisonner les plats.
* **Persil frisé :** Plus décoratif, utilisé souvent comme garniture.

### 2.4 Thym (Thymus vulgaris)

Le thym est une plante aromatique vivace qui se cultive très bien en pot et qui résiste aux conditions difficiles, comme la sécheresse. Il est parfait pour agrémenter les viandes, les soupes, et les marinades.

* **Exposition :** Plein soleil.
* **Arrosage :** Arrosage modéré, le thym tolère bien les périodes de sécheresse.
* **Conseils de culture :** Semez les graines à la surface du sol, puis recouvrez-les légèrement. Le thym préfère un sol bien drainé et léger.

#### Variétés recommandées :

* **Thym citronné :** Très parfumé avec une touche de citron, parfait pour les poissons et les volailles.
* **Thym classique :** Incontournable dans la cuisine méditerranéenne.

### 2.5 Ciboulette (Allium schoenoprasum)

La ciboulette est une plante facile à cultiver, idéale pour assaisonner les salades, les soupes et les plats à base de fromage. Elle pousse bien en pot et offre une floraison décorative au printemps.

* **Exposition :** Plein soleil à mi-ombre.
* **Arrosage :** Arrosage régulier, surtout en été.
* **Conseils de culture :** Semez les graines de ciboulette à 1 cm de profondeur, en les espaçant de 5 à 10 cm. Elle se plait dans des pots peu profonds.

#### Variétés recommandées :

* **Ciboulette commune :** La variété la plus courante, à saveur douce.
* **Ciboulette à fleurs violettes :** Produit de jolies fleurs comestibles et décoratives.

### 2.6 Coriandre (Coriandrum sativum)

La coriandre est une herbe aux multiples usages, surtout dans les cuisines asiatique et mexicaine. Elle pousse rapidement et peut être cultivée en pot avec un sol bien drainé.

* **Exposition :** Plein soleil à mi-ombre.
* **Arrosage :** Arrosage régulier, surtout en période de chaleur.
* **Conseils de culture :** Semez les graines directement en pot à une profondeur de 1 à 2 cm. Évitez les températures trop élevées, car la coriandre monte rapidement en graines sous la chaleur.

#### Variétés recommandées :

* **Coriandre ‘Leisure’ :** Variété à feuilles larges, idéale pour la cuisine.
* **Coriandre ‘Santo’ :** Résistante aux montées en graines.

![Astuces pour réussir la culture des herbes aromatiques en pot](/img/img-article-astuces-reussir-culture-herbes-aromatiques-pot.png "Astuces pour réussir la culture des herbes aromatiques en pot")

## 3. Astuces pour réussir la culture des herbes aromatiques en pot

Pour réussir la culture de vos herbes aromatiques en pot, il est important de respecter quelques règles de base en matière de semis, d’arrosage et d’entretien.

### 3.1 Choisir le bon pot

Le choix du pot est essentiel pour la culture des herbes aromatiques. Voici quelques critères à considérer :

* **Taille :** Utilisez des pots d’au moins 20 à 30 cm de diamètre pour permettre aux racines de bien se développer. Pour des herbes comme la menthe ou le basilic, préférez des pots plus grands.
* **Drainage :** Assurez-vous que vos pots disposent de trous de drainage pour éviter que l’eau ne stagne et ne provoque la pourriture des racines. Placez une couche de gravier ou de billes d’argile au fond des pots pour améliorer le drainage.

### 3.2 Utiliser un terreau de qualité

Les herbes aromatiques prospèrent dans un terreau léger et bien drainé. Utilisez un terreau universel de bonne qualité ou un terreau spécifique pour plantes aromatiques. Vous pouvez également ajouter un peu de sable pour améliorer le drainage.

### 3.3 Semis et espacement des graines

Respectez les distances de plantation recommandées sur les sachets de graines pour éviter que les plantes ne se gênent mutuellement. Les herbes aromatiques ont besoin d’espace pour bien se développer. Si vous semez plusieurs variétés dans un même pot, veillez à ce que chaque plante dispose de suffisamment d'espace.

### 3.4 Arrosage et exposition

L’arrosage est crucial pour la culture en pot. Voici quelques conseils pour bien arroser vos herbes aromatiques :

* **Fréquence d’arrosage :** Arrosez vos pots régulièrement, surtout en été. Veillez à ce que le sol ne sèche pas complètement, mais évitez les excès d’eau.
* **Exposition au soleil :** La plupart des herbes aromatiques préfèrent une exposition en plein soleil. Assurez-vous que vos pots reçoivent au moins 4 à 6 heures de lumière directe par jour.

### 3.5 Engrais et nutriments

Les herbes en pot bénéficient d’un apport en nutriments supplémentaire. Utilisez un engrais liquide pour plantes aromatiques toutes les deux à trois semaines pendant la période de croissance. Un engrais riche en azote favorisera la production de feuilles.

### 3.6 Taille et récolte

Pour encourager une croissance continue, récoltez régulièrement les feuilles de vos herbes aromatiques. Évitez de couper plus d’un tiers de la plante à la fois pour ne pas l’affaiblir. Coupez les fleurs si vous ne souhaitez pas que la plante monte en graines, car cela ralentit la production de feuilles.

## Conclusion

Cultiver des herbes aromatiques à partir de graines sur votre balcon ou terrasse est une excellente manière de profiter d’une récolte fraîche et savoureuse toute l’année. En choisissant les variétés adaptées comme le basilic, la menthe ou la coriandre, et en suivant les bonnes pratiques de semis et d’entretien, vous pouvez facilement transformer un petit espace en un véritable jardin aromatique. Que vous soyez un jardinier débutant ou expérimenté, la culture en pot vous permet d’accéder à des herbes aromatiques toujours à portée de main, pour sublimer vos plats au quotidien.
