---
title: "Les meilleures plantes méditerranéennes à cultiver en pot : Idées pour
  balcons et terrasses"
meta:
  keywords:
    - plantes méditerranéennes en pot
    - balcon méditerranéen
    - terrasse méditerranéenne
    - cultiver olivier en pot
    - citronnier en pot
    - lavande balcon
    - plantes résistantes à la sécheresse
    - jardin méditerranéen
    - entretien plantes méditerranéennes
  description: Apportez une touche méditerranéenne à votre balcon ou terrasse !
    Découvrez les meilleures plantes méditerranéennes à cultiver en pot, des
    conseils d'entretien pour réussir la culture d'oliviers, citronniers,
    lavande et autres plantes typiques du sud.
image: /img/img-cover-plantes-mediterraneennes.webp
summary: >-
  Les plantes méditerranéennes, avec leur feuillage persistant, leurs couleurs
  éclatantes et leurs parfums envoûtants, évoquent immédiatement la chaleur et
  la douceur de vivre des régions du sud. Cultiver ces plantes en pot sur un
  balcon ou une terrasse permet d’apporter une touche d’exotisme et de soleil à
  votre espace extérieur, même si vous n’habitez pas dans une région au climat
  méditerranéen. Dans cet article, nous vous présentons les meilleures plantes
  méditerranéennes à cultiver en pot, ainsi que des conseils pratiques pour les
  entretenir et en profiter pleinement.


  Pourquoi choisir des plantes méditerranéennes pour votre balcon ou terrasse ?


  Les plantes méditerranéennes sont parfaites pour les balcons et terrasses pour plusieurs raisons.
slug: les-meilleures-plantes-m-diterran-ennes-cultiver-en-pot-id-es-pour-balcons-et-terrasses
lastmod: 2024-11-11T11:06:05.599Z
category: plantes-mediterraneenes/_index
published: 2024-11-11T10:22:00.000Z
weight: 0
administrable: true
url: fiches-conseils/plantes-mediterraneenes/les-meilleures-plantes-m-diterran-ennes-cultiver-en-pot-id-es-pour-balcons-et-terrasses
kind: page
---
Les plantes méditerranéennes, avec leur feuillage persistant, leurs couleurs éclatantes et leurs parfums envoûtants, évoquent immédiatement la chaleur et la douceur de vivre des régions du sud. Cultiver ces plantes en pot sur un balcon ou une terrasse permet d’apporter une touche d’exotisme et de soleil à votre espace extérieur, même si vous n’habitez pas dans une région au climat méditerranéen. Dans cet article, nous vous présentons les meilleures plantes méditerranéennes à cultiver en pot, ainsi que des conseils pratiques pour les entretenir et en profiter pleinement.

![Pourquoi choisir des plantes méditerranéennes pour votre balcon ou terrasse ?](/img/img-articles-plantes-mediterraneennes-pourquoi.webp "Pourquoi choisir des plantes méditerranéennes pour votre balcon ou terrasse ?")

## 1. Pourquoi choisir des plantes méditerranéennes pour votre balcon ou terrasse ?

Les plantes méditerranéennes sont parfaites pour les balcons et terrasses pour plusieurs raisons :

### 1.1 Résistance à la sécheresse

La plupart des plantes méditerranéennes sont adaptées aux conditions de sécheresse, ce qui les rend parfaites pour la culture en pot. Elles nécessitent moins d’arrosage que d’autres plantes et peuvent tolérer les périodes de chaleur sans problème.

### 1.2 Esthétique et ambiance méditerranéenne

Ces plantes sont synonymes de beauté naturelle. Avec leur feuillage argenté, leurs fleurs colorées et leur parfum envoûtant, elles créent une ambiance relaxante et élégante, parfaite pour un espace de détente en extérieur.

### 1.3 Facilité d'entretien

Les plantes méditerranéennes sont souvent robustes et peu exigeantes en termes d’entretien. Une fois bien installées, elles demandent peu de soins et résistent bien aux maladies, ce qui les rend idéales pour les jardiniers débutants ou pour ceux qui n’ont pas beaucoup de temps à consacrer au jardinage.

![Les meilleures plantes méditerranéennes à cultiver en pot](/img/img-articles-plantes-mediterraneennes-meilleurs.webp "Les meilleures plantes méditerranéennes à cultiver en pot")

## 2. Les meilleures plantes méditerranéennes à cultiver en pot

Voici une sélection des plantes méditerranéennes les plus adaptées à la culture en pot, qui transformeront votre balcon ou terrasse en un véritable jardin méditerranéen.

### 2.1 Olivier (Olea europaea)

L’olivier est sans doute la plante méditerranéenne la plus emblématique. Ce petit arbre au feuillage argenté est très décoratif et s’adapte parfaitement à la culture en pot. Il peut être cultivé dans des régions plus fraîches, à condition d'être protégé en hiver.

* **Exposition :** Plein soleil.
* **Type de pot :** Grand pot en terre cuite d’au moins 40 à 50 cm de diamètre.
* **Entretien :** L’olivier nécessite un sol bien drainé. Arrosez-le modérément, car il tolère bien la sécheresse. Pensez à le rentrer ou à le protéger des gelées en hiver si vous vivez dans une région froide.
* **Taille :** Taillez régulièrement pour maintenir une forme compacte et favoriser la ramification.
* **Atout :** En plus d’être décoratif, il est possible de récolter des olives sur des arbres bien établis, même en pot.

### 2.2 Laurier-rose (Nerium oleander)

Le laurier-rose est une plante incontournable des jardins méditerranéens. Ses fleurs colorées (rose, blanc, rouge, jaune) sont particulièrement belles et fleurissent tout au long de l’été. Très résistant, le laurier-rose se prête bien à la culture en pot.

* **Exposition :** Plein soleil.
* **Type de pot :** Pot profond d’au moins 30 cm de diamètre, avec un bon drainage.
* **Entretien :** Arrosez régulièrement pendant la période de floraison, mais laissez le sol sécher entre deux arrosages. Taillez légèrement après la floraison pour encourager une nouvelle croissance et conserver une forme compacte.
* **Précaution :** Le laurier-rose est toxique en cas d’ingestion. Placez-le hors de portée des enfants et des animaux.
* **Atout :** Floraison généreuse et résistance à la sécheresse.

### 2.3 Citronnier (Citrus limon)

Le citronnier est une autre plante méditerranéenne qui s’adapte parfaitement à la culture en pot. Avec son feuillage brillant, ses fleurs parfumées et ses fruits jaunes éclatants, il apportera une touche d’élégance à votre balcon ou terrasse.

* **Exposition :** Plein soleil.
* **Type de pot :** Grand pot avec un bon drainage, d’environ 40 à 50 cm de diamètre.
* **Entretien :** Le citronnier a besoin d’un arrosage régulier et d’un sol riche en nutriments. Fertilisez-le tous les mois pendant la période de croissance avec un engrais spécial agrumes.
* **Taille :** Taillez légèrement au printemps pour encourager la ramification.
* **Protection hivernale :** Rentrez-le à l’intérieur dans un endroit lumineux et frais si vous vivez dans une région froide.
* **Atout :** Vous pouvez récolter vos propres citrons tout en profitant d’un parfum délicieux émanant des fleurs.

### 2.4 Lavande (Lavandula angustifolia)

La lavande est une plante méditerranéenne très appréciée pour son parfum envoûtant et ses fleurs violettes qui attirent les pollinisateurs. Elle est idéale pour les petits espaces comme les balcons et terrasses grâce à sa croissance compacte.

* **Exposition :** Plein soleil.
* **Type de pot :** Pot d’au moins 30 cm de diamètre, avec un sol bien drainé.
* **Entretien :** Arrosez modérément et assurez-vous que le sol est bien drainé, car la lavande n’aime pas les excès d’eau. Taillez après la floraison pour conserver une forme compacte.
* **Atout :** La lavande est une plante rustique qui peut également être utilisée pour des infusions ou des sachets parfumés.

### 2.5 Romarin (Rosmarinus officinalis)

Le romarin est une plante aromatique typiquement méditerranéenne qui se prête bien à la culture en pot. En plus de ses qualités culinaires, le romarin est très décoratif et résistant à la sécheresse.

* **Exposition :** Plein soleil.
* **Type de pot :** Pot profond d’environ 30 cm de diamètre, avec un bon drainage.
* **Entretien :** Arrosez avec modération, surtout en hiver. Taillez légèrement après la floraison pour favoriser une nouvelle croissance.
* **Atout :** Son feuillage persistant apporte une touche de verdure toute l’année et il peut être utilisé en cuisine pour parfumer les plats.

### 2.6 Bougainvillier (Bougainvillea)

Le bougainvillier est une plante grimpante méditerranéenne qui produit des fleurs éclatantes de différentes couleurs (rose, violet, blanc, orange). Il est parfait pour apporter une explosion de couleur à votre balcon ou terrasse.

* **Exposition :** Plein soleil.
* **Type de pot :** Grand pot ou bac, avec un tuteur pour guider la croissance.
* **Entretien :** Arrosez régulièrement en été, mais laissez le sol sécher entre deux arrosages. Taillez au printemps pour encourager une floraison abondante.
* **Atout :** Sa floraison généreuse et colorée égaye instantanément n’importe quel espace extérieur.

### 2.7 Agave (Agave americana)

L’agave est une plante méditerranéenne succulente qui s’adapte parfaitement à la culture en pot. Son feuillage graphique et sa résistance à la sécheresse en font une plante idéale pour les balcons et terrasses.

**Exposition :** Plein soleil.

**Type de pot :** Pot d’au moins 30 cm de diamètre, avec un sol bien drainé.

**Entretien :** Arrosez modérément, surtout pendant la période de croissance. Assurez-vous que le pot a un bon drainage pour éviter l’accumulation d'eau.

**Atout :** L’agave demande très peu d’entretien et tolère bien les fortes chaleurs.

![Conseils pour réussir la culture des plantes méditerranéennes en pot](/img/img-articles-plantes-mediterraneennes-conseils.webp "Conseils pour réussir la culture des plantes méditerranéennes en pot")

## 3. Conseils pour réussir la culture des plantes méditerranéennes en pot

Voici quelques astuces pour optimiser la culture de vos plantes méditerranéennes en pot et garantir leur bonne croissance.

### 3.1 Choisir le bon pot et le bon substrat

Le choix du pot est essentiel pour la culture en pot. Optez pour des pots en terre cuite ou en plastique avec des trous de drainage pour éviter la stagnation de l'eau. Pour le substrat, utilisez un terreau bien drainé, adapté aux plantes méditerranéennes, que vous pouvez enrichir avec du compost ou du sable pour améliorer le drainage.

### 3.2 Arrosage adapté

Bien que les plantes méditerranéennes soient résistantes à la sécheresse, elles nécessitent un arrosage régulier pendant leur période de croissance. Toutefois, il est important de laisser sécher la surface du sol entre deux arrosages, surtout pour des plantes comme l’olivier, la lavande ou le romarin, qui n’apprécient pas les sols détrempés.

### 3.3 Exposition au soleil

La plupart des plantes méditerranéennes aiment le plein soleil. Assurez-vous que votre balcon ou terrasse bénéficie d'une bonne exposition pour garantir une floraison abondante et une croissance vigoureuse.

### 3.4 Taille et entretien

La taille régulière des plantes méditerranéennes permet de contrôler leur croissance, de favoriser la ramification et d'encourager une nouvelle floraison. Taillez après la période de floraison, et retirez les fleurs fanées pour prolonger la floraison.

### 3.5 Protection hivernale

Certaines plantes méditerranéennes, comme le citronnier ou l’olivier, peuvent être sensibles au froid. Si vous vivez dans une région où les températures descendent en dessous de zéro, pensez à rentrer vos plantes à l’intérieur ou à les protéger avec un voile d’hivernage.

## Conclusion

Cultiver des plantes méditerranéennes en pot sur un balcon ou une terrasse est une excellente façon d’apporter chaleur et exotisme à votre espace extérieur. Des plantes comme l'olivier, le laurier-rose, ou encore le citronnier sont à la fois esthétiques et pratiques. En suivant quelques conseils simples, notamment en matière de drainage, d’arrosage et de taille, vous pourrez créer un véritable coin de paradis méditerranéen chez vous, même dans un espace restreint. Avec leur faible besoin en entretien et leur grande résistance, ces plantes sont parfaites pour tous ceux qui souhaitent profiter de la beauté des jardins du sud sans les contraintes.
