---
category: graines-de-fleurs-et-bulbes/bulbes-a-fleurs/_index
meta:
  keywords:
  - plantes
  - bulbes
  - planter
image: /img/background-plante_exterieur.webp
summary: <p>La gamme des bulbes de printemps est large et offre de multiples
  possibilités au jardin. Premières fleurs du printemps, les bulbes sont faciles
  à cultiver et à réussir. Ils se plieront à toutes vos envies, choisissez juste
  les espèces les plus appropriées. Massif, rocaille, pot, sous-bois… suivez le
  guide pour réussir votre jardin de bulbes de printemps !</p><p>Qu’il s’agisse
  de fleurir une rocaille ensoleillée ou un sous-bois sombre, il existe à coup
  sûr un bulbe à fleurs adéquat ! Compte tenu de leur temps de végétation assez
  court, les bulbes de printemps vous permettent de "boucher les trous" et
  d’assurer un décor printanier très varié. Sachez que seul l’effet de masse
  donnant de beaux résultats, plantez-les par groupe d'au moins 25 à 50.
  Beaucoup d’espèces se naturalisant bien, vous garderez durant de nombreuses
  années votre décor !</p>
related: null
slug: 2024-08-20-plantes-a-bulbes-quand-les-planter
lastmod: 2024-08-29T12:10:28.496Z
title: Plantes à bulbes, quand les planter ?
published: 2024-08-20T21:35:00.000Z
kind: page
administrable: true
url: fiches-conseils/graines-de-fleurs-et-bulbes/bulbes-a-fleurs/2024-08-20-plantes-a-bulbes-quand-les-planter
---
<figure class="image image-style-side">

![](/img/jardin.webp)

</figure>

## Sponsorisé par Algoflash

Les arbres d'ornement constituent l'ossature du jardin et <strong>devront nécessairement s'harmoniser entre eux et avec ceux du paysage environnant</strong>. Si beaucoup d'arbres atteignent de grandes dimensions, il en existe de faible développement et cela permet de trouver des arbres pour tous les types de jardins, grands ou petits. La plupart ne demandent que peu d'entretien : une fois plantés, les arbres et arbustes d'ornement se développeront plus ou moins vite en fonction de la qualité du sol et de l'eau dont ils disposent.

Vous pouvez mettre en terre les arbres et arbustes tout au long de l'année, hormis pendant les périodes de gel. La meilleure époque de plantation est en octobre-novembre ou en mars-avril.Planté en automne, l'arbre ou l'arbuste a bien le temps de s'enraciner et comme dit le dicton "A la Sainte-Catherine, tout bois prend racine".&nbsp;</p><p>De plus, durant cette période de l'année, les arbres et arbustes vous sont proposés en racines nues, ils sont alors moins cher (plus léger pour le transport), et ont un meilleur taux de reprise.

Pour les essences sensibles au froid, les arbustes à floraison estivale, dans les régions à hiver rigoureux, préférez le printemps pour planter.

## Quand planter vos arbres et arbustes d'ornement?

Vous pouvez mettre en terre les arbres et arbustes tout au long de l'année, hormis pendant les périodes de gel. La meilleure époque de plantation est en octobre-novembre ou en mars-avril.

Planté en automne, l'arbre ou l'arbuste a bien le temps de s'enraciner et comme dit le dicton "A la Sainte-Catherine, tout bois prend racine". De plus, durant cette période de l'année, les arbres et arbustes vous sont proposés en racines nues, ils sont alors moins cher (plus léger pour le transport), et ont un meilleur taux de reprise.

Pour les essences sensibles au froid, les arbustes à floraison estivale, dans les régions à hiver rigoureux, préférez le printemps pour planter.

### La plantation et la taille des arbres et arbustes d'ornement:

* La plantation aura lieu en automne de préférence pour profiter de la terre encore chaude et des pluies qui assurent les besoins en eau de la jeune plante.
* La taille n'est pas indispensable selon le type d'arbre ou d'arbuste. Généralement le port naturel ne réclame aucune taille. Toutefois il est conseillé d'éliminer les branches malades, celles qui sont mal placées, et peuvent devenir gênantes voire dangereuses.

## Comment planter vos arbres et arbustes d'ornement?

Vous pouvez mettre en terre les arbres et arbustes tout au long de l'année, hormis pendant les périodes de gel. La meilleure époque de plantation est en octobre-novembre ou en mars-avril.Planté en automne, l'arbre ou l'arbuste a bien le temps de s'enraciner et comme dit le dicton "A la Sainte-Catherine, tout bois prend racine".

De plus, durant cette période de l'année, les arbres et arbustes vous sont proposés en racines nues, ils sont alors moins cher (plus léger pour le transport), et ont un meilleur taux de reprise.

Pour les essences sensibles au froid, les arbustes à floraison estivale, dans les régions à hiver rigoureux, préférez le printemps pour planter.

* **Décaissez sur 1 m3** pour remplir ensuite avec de la terre végétale. Un amendement de terreau et une fumure (corne broyée) sont souvent à conseiller. Optez aussi pour le stimulateur de racines Agrosil d'ALgoflash, qui favorise l'enracinement et la reprise de la plante, à utiliser à la plantation puis à l'entretien.
* **Plantez le tuteur** à au moins 60 cm de profondeur. Il sera attaché au tronc avec un lien réglable.
* **Plantez** en présentant le collet de l'arbre juste au niveau du sol puis tasser la terre.
* **Créez une cuvette** au bord de l'arbre et arrosez abondamment.

> **Remarque** : Les arbres en motte et les conifères seront haubanés, c'est-à-dire qu'ils sont maintenus droits grâce à plusieurs câbles tendus.

<figure class="image">

![](/img/planter.webp)

<figcaption>planter</figcaption>

</figure>

## Quels arbres planter dans son jardin?

Planter un arbre dans son jardin n'est pas un acte anodin, mais bien une décision qui vous implique sur des dizaines d'années ! Pour vous aider dans votre choix, lisez nos conseils <span style="color:#61D7A3;">Bien choisir un arbre</span>.
Suivez nos critères de choix d'un arbre et arbuste d'ornement :

1 - Son développement :

* Grand (marronnier, platane, peuplier...) ou petit (aubépine, saule, prunus,...)<br>Rapide (tulipier, paulownia, tilleul,...) ou lent (chêne, hêtre,...)

2 - Son port :

* Forme pyramidale : nombreux conifères, magnolia à grandes fleurs, liquidambar,…
* Forme fastigiée : les peupliers, les cyprès, le charme fastigié,…
* Forme ronde : les marronniers, la plupart des érables, le cédrèle (ou Acajou de Chine), les tilleuls, les sorbiers..
* Forme étalée : l'albizia, le cèdre du Liban,…
* Forme pleureuse : saule pleureur, hêtre pleureur, sophora pleureur

<figure class="image">

![](/img/autre_jardin.webp)

</figure>

3 - Son feuillage :

* Caduc ou persistant. Pensez aux belles couleurs automnales des feuillages caducs (particulièrement chez les érables, les hêtres, les liquidambars, les charmes, les tulipiers,...).
* Vert ou coloré (hêtre pourpre, orme doré,...)

4 - Sa floraison :

L'époque, la couleur et l'odeur des fleurs, voire des fruits ne sont pas à négliger.

## Entretien des arbres et arbustes d'ornement

Après la plantation des arbres et arbustes d'ornement, la taille et l'entretien de ces végétaux sont importants pour les garder beaux et sains :

* **Arrosez régulièrement** après la plantation.
* Les grands arbres ont besoin d'être **taillés de temps à autre**. Faites appel à un élagueur pratiquant la taille douce ou raisonnée afin d'éviter d'avoir un arbre massacré. L'élagage permet de supprimer des branches gênantes ou inesthétiques et de renforcer l'arbre.
* Si des lichens ou de la mousse s'installent, <strong>brossez régulièrement le tronc</strong> des arbres afin d'éviter l'installation de champignons.
* Traitez si besoin contre les parasites.
