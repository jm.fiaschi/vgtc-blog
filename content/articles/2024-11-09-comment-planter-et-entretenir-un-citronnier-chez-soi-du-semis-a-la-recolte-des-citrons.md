---
title: "Comment planter et entretenir un citronnier chez soi : Du semis à la
  récolte des citrons"
meta:
  description: Apprenez comment planter et entretenir un citronnier chez vous, en
    pot ou en pleine terre, avec nos conseils sur l'arrosage, la taille, la
    fertilisation et la protection hivernale. Profitez d'une récolte de citrons
    frais toute l'année, même en climat froid.
  keywords:
    - planter un citronnier
    - entretien citronnier en pot
    - culture citronnier en intérieur
    - récolte des citrons
    - variétés de citronniers
    - arrosage citronnier
    - taille citronnier
    - engrais pour citronnier
    - protection hivernale citronnier
    - citronnier Meyer
image: /img/img-cover-planter-entretenir-citronnier.webp
summary: >-
  Le citronnier, avec ses fruits ensoleillés et ses fleurs parfumées, est l’un
  des agrumes les plus appréciés pour la culture domestique. Que ce soit en
  pleine terre dans les régions au climat doux ou en pot pour les climats plus
  froids, il est tout à fait possible de faire pousser un citronnier chez soi.
  Avec des soins adaptés, un citronnier peut non seulement embellir votre jardin
  ou terrasse, mais aussi vous offrir de délicieux citrons tout au long de
  l’année. Dans cet article, nous détaillerons les étapes essentielles pour
  planter et entretenir un citronnier, du semis à la récolte, en couvrant aussi
  bien la culture en pleine terre qu’en pot.


  Choisir la variété de citronnier adaptée


  Avant de planter votre citronnier, il est important de choisir la bonne variété en fonction de vos besoins et de votre environnement. Toutes les variétés ne conviennent pas à tous les climats, et certaines sont mieux adaptées à la culture en pot.
slug: comment-planter-et-entretenir-un-citronnier-chez-soi-du-semis-la-r-colte-des-citrons
lastmod: 2024-11-09T16:53:53.056Z
category: fruitiers/agrumes/_index
published: 2024-11-09T16:29:00.000Z
weight: 0
administrable: true
url: fiches-conseils/fruitiers/agrumes/comment-planter-et-entretenir-un-citronnier-chez-soi-du-semis-la-r-colte-des-citrons
kind: page
---
Le citronnier, avec ses fruits ensoleillés et ses fleurs parfumées, est l’un des agrumes les plus appréciés pour la culture domestique. Que ce soit en pleine terre dans les régions au climat doux ou en pot pour les climats plus froids, il est tout à fait possible de faire pousser un citronnier chez soi. Avec des soins adaptés, un citronnier peut non seulement embellir votre jardin ou terrasse, mais aussi vous offrir de délicieux citrons tout au long de l’année. Dans cet article, nous détaillerons les étapes essentielles pour planter et entretenir un citronnier, du semis à la récolte, en couvrant aussi bien la culture en pleine terre qu’en pot.

![Choisir la variété de citronnier adaptée](/img/img-articles-variete-citronnier.webp "Choisir la variété de citronnier adaptée")

## 1. Choisir la variété de citronnier adaptée

Avant de planter votre citronnier, il est important de choisir la bonne variété en fonction de vos besoins et de votre environnement. Toutes les variétés ne conviennent pas à tous les climats, et certaines sont mieux adaptées à la culture en pot.

### 1.1 Le citronnier 'Meyer'

Le citronnier 'Meyer' est l’une des variétés les plus populaires pour la culture domestique. Il produit des fruits légèrement plus doux que les citrons traditionnels, et son arbre est plus petit, ce qui le rend parfait pour la culture en pot. Il est aussi plus résistant au froid que les autres variétés de citronnier.

* **Avantages :** Résistant au froid, adapté aux petits espaces, floraison et fructification toute l’année.
* **Idéal pour :** Culture en pot ou en pleine terre dans les climats doux.

### 1.2 Le citronnier 'Eureka'

Le citronnier 'Eureka' est une variété très productive qui offre des citrons acides, parfaits pour la cuisine. Cet arbre est un peu plus grand que le Meyer et préfère les climats chauds.

* **Avantages :** Grande production de fruits, croissance rapide.
* **Idéal pour :** Climat méditerranéen ou tropical, culture en pleine terre.

### 1.3 Le citronnier 'Limonero Verna'

Le citronnier 'Limonero Verna' est une variété espagnole qui produit des citrons plus gros, généralement utilisés dans les cuisines méditerranéennes. Il est également adapté aux climats chauds.

* **Avantages :** Fruits de grande taille, floraison plusieurs fois par an.
* **Idéal pour :** Régions méditerranéennes, culture en pleine terre ou en pot si bien taillé.

![Planter un citronnier : Les étapes clés](/img/img-articles-planter-citronnier.webp "Planter un citronnier : Les étapes clés")

## 2. Planter un citronnier : Les étapes clés

Que vous décidiez de planter un citronnier en pleine terre ou en pot, les étapes de plantation sont essentielles pour assurer une bonne croissance et une future fructification. Voici les différentes étapes à suivre.

### 2.1 Planter un citronnier en pot

La culture en pot est idéale pour les régions aux hivers froids, car vous pouvez rentrer le citronnier à l'intérieur pour le protéger du gel.

**Choix du pot :** Optez pour un pot de 40 à 60 cm de diamètre pour permettre aux racines de bien se développer. Assurez-vous que le pot dispose de trous de drainage pour éviter la stagnation de l'eau.

**Substrat :** Utilisez un mélange de terreau pour agrumes, qui offre un bon drainage tout en retenant suffisamment d'humidité. Un mélange de terreau, de sable et de terre de jardin est également une option.

**Plantation :** Placez une couche de gravier ou de billes d’argile au fond du pot pour améliorer le drainage. Plantez le citronnier à la même profondeur que celle du pot d'origine, puis remplissez le pot avec du substrat, en tassant légèrement autour des racines.

### 2.2 Planter un citronnier en pleine terre

Si vous vivez dans une région au climat doux, vous pouvez planter un citronnier directement en pleine terre.

**Emplacement :** Choisissez un emplacement en plein soleil, à l'abri des vents forts. Le citronnier a besoin de 6 à 8 heures de soleil par jour pour bien se développer.

**Préparation du sol :** Le sol doit être bien drainé. Si votre sol est lourd ou argileux, ajoutez du sable ou du gravier pour améliorer le drainage. Le pH du sol doit être légèrement acide, idéalement entre 5,5 et 6,5.

**Plantation :** Creusez un trou deux fois plus large que la motte du citronnier et d’une profondeur équivalente à la motte. Placez l'arbre dans le trou, remplissez avec du terreau, et tassez légèrement. Arrosez généreusement après la plantation.

![L'entretien d’un citronnier : Les gestes à adopter](/img/img-articles-entretien-citronnier.webp "L'entretien d’un citronnier : Les gestes à adopter")

## 3. L'entretien d’un citronnier : Les gestes à adopter

Une fois planté, le citronnier nécessite des soins spécifiques pour bien s’épanouir, qu’il soit en pot ou en pleine terre.

### 3.1 L’arrosage

L’arrosage est crucial pour la bonne santé de votre citronnier. Il est important de maintenir un sol légèrement humide, sans pour autant laisser les racines baigner dans l'eau.

**En pot :** Arrosez régulièrement, surtout pendant la période de croissance (printemps et été), mais laissez le sol sécher légèrement entre deux arrosages. En hiver, réduisez les arrosages, surtout si l’arbre est à l'intérieur.

**En pleine terre :** Arrosez plus fréquemment en été et pendant les périodes de sécheresse. Assurez-vous que l'eau pénètre bien le sol et atteigne les racines.

### 3.2 La fertilisation

Les citronniers sont des arbres gourmands en nutriments, notamment en azote. Une fertilisation régulière est nécessaire pour soutenir la croissance et la production de fruits.

**Engrais :** Utilisez un engrais spécifique pour agrumes, riche en azote, phosphore et potassium, que vous appliquerez toutes les deux à quatre semaines pendant la saison de croissance (de mars à septembre).

**Apport de matière organique :** Vous pouvez également ajouter du compost ou du fumier décomposé autour de l'arbre une fois par an, au printemps, pour enrichir le sol en nutriments.

### 3.3 La taille du citronnier

La taille est une étape importante pour maintenir la forme compacte de l’arbre et favoriser une meilleure production de fruits.

**Quand tailler ? :** Taillez le citronnier au début du printemps, juste avant que la nouvelle croissance commence. Vous pouvez également enlever les branches mortes ou malades à tout moment de l'année.

**Comment tailler ? :** Supprimez les branches faibles ou qui poussent vers l’intérieur de l’arbre pour améliorer la circulation de l’air et la pénétration de la lumière. Taillez légèrement les branches principales pour stimuler la ramification.

### 3.4 La protection hivernale

Les citronniers ne tolèrent pas bien le gel. Si vous vivez dans une région où les températures hivernales descendent en dessous de 5°C, il est essentiel de protéger votre citronnier.

**En pot :** Rentrez le citronnier à l'intérieur avant les premières gelées. Placez-le dans une pièce lumineuse et fraîche, comme une véranda, et réduisez les arrosages pendant l'hiver.

**En pleine terre :** Couvrez l’arbre avec un voile d’hivernage pour le protéger des températures basses. Vous pouvez également pailler autour du tronc pour protéger les racines du gel.

![Du semis à la récolte des citrons](/img/img-articles-semis-citronnier.webp "Du semis à la récolte des citrons")

## 4. Du semis à la récolte des citrons

La culture d’un citronnier à partir de graines est une expérience enrichissante, mais il faut savoir que les arbres issus de semis prennent plus de temps à produire des fruits que ceux greffés.

### 4.1 Semer un citronnier à partir de graines

Semer un citronnier à partir de graines est simple, mais il est important de savoir que cela demande de la patience. Voici les étapes pour démarrer un citronnier à partir de graines :

**Préparation des graines :** Prélevez des graines d’un citron frais. Rincez-les pour enlever toute pulpe, puis laissez-les sécher pendant 24 heures.

**Plantation des graines :** Plantez les graines dans un petit pot rempli de terreau léger, à environ 1 cm de profondeur. Arrosez légèrement et placez le pot dans un endroit chaud et lumineux, mais à l’abri de la lumière directe du soleil.

**Germination :** Les graines de citronnier mettent généralement 2 à 4 semaines pour germer. Maintenez le sol humide mais non détrempé pendant cette période.

**Repiquage :** Lorsque les jeunes plants ont quelques feuilles, vous pouvez les repiquer dans des pots plus grands. Soyez patient, car il faudra plusieurs années avant que ces jeunes citronniers ne commencent à produire des fruits.

### 4.2 La première fructification

Les citronniers issus de pépins peuvent mettre 5 à 15 ans à produire des fruits, tandis que les citronniers greffés produisent des citrons après 3 à 5 ans. La première floraison se produit souvent au printemps, avec des fleurs blanches très parfumées.

**Attente de la récolte :** Les citrons prennent plusieurs mois à mûrir, généralement entre 6 et 9 mois après la floraison. Vous pouvez récolter les citrons lorsque leur peau est bien jaune et qu’ils se détachent facilement de la branche.

### 4.3 Récolter les citrons

La récolte des citrons peut se faire tout au long de l’année, mais elle est généralement plus abondante en hiver et au début du printemps. Voici quelques conseils pour une récolte optimale :

**Quand récolter :** Les citrons sont prêts à être récoltés lorsqu'ils sont bien jaunes et que leur peau est légèrement souple. Si vous attendez trop longtemps, les fruits risquent de devenir trop mûrs et de perdre de leur saveur.

**Comment récolter :** Utilisez un sécateur ou une paire de ciseaux pour couper les fruits, en laissant un petit morceau de tige attaché au citron. Évitez de tirer sur le fruit, car cela peut endommager les branches.

![Problèmes courants et solutions](/img/img-articles-probleme-citronnier.webp "Problèmes courants et solutions")

## 5. Problèmes courants et solutions

Même avec les meilleurs soins, un citronnier peut parfois rencontrer des problèmes. Voici quelques soucis courants et des solutions pour les résoudre.

### 5.1 Chute des feuilles

La chute des feuilles est souvent due à un excès ou un manque d’eau, ou à une mauvaise exposition.

**Solution :** Vérifiez l’arrosage et ajustez-le si nécessaire. Assurez-vous que le pot a un bon drainage et que le citronnier reçoit suffisamment de lumière.

### 5.2 Cochenilles et pucerons

Les cochenilles et pucerons sont des parasites fréquents sur les citronniers. Ils se nourrissent de la sève et affaiblissent l’arbre.

**Solution :** Utilisez un jet d’eau pour déloger les parasites, puis appliquez du savon noir ou de l’huile de neem pour traiter les infestations.

### 5.3 Jaunissement des feuilles

Le jaunissement des feuilles peut être causé par une carence en nutriments, notamment en azote ou en fer.

**Solution :** Appliquez un engrais spécifique pour agrumes et vérifiez que le sol est légèrement acide. Si le problème persiste, vous pouvez ajouter un supplément de fer.

## Conclusion

Planter et entretenir un citronnier chez soi est une activité gratifiante qui peut vous apporter de nombreux avantages, qu’il s’agisse de l’esthétique de l’arbre, de son parfum enivrant ou de la récolte de citrons frais. Que vous choisissiez de le cultiver en pot ou en pleine terre, il est essentiel de lui offrir des soins adaptés, notamment en matière d’arrosage, de fertilisation et de protection contre le froid. Avec un peu de patience et d’attention, votre citronnier vous récompensera avec de beaux fruits jaunes éclatants, apportant une touche de soleil à votre maison ou votre jardin.
