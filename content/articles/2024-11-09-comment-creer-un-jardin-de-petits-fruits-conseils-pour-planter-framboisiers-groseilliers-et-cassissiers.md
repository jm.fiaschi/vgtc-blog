---
title: "Comment créer un jardin de petits fruits : Conseils pour planter
  framboisiers, groseilliers et cassissiers"
meta:
  description: Découvrez comment créer un jardin de petits fruits et cultiver
    framboisiers, groseilliers et cassissiers pour profiter de fruits frais et
    savoureux chez vous. Suivez nos conseils pour réussir la plantation,
    l'entretien et la récolte de ces arbustes fruitiers.
  keywords:
    - jardin de petits fruits
    - cultiver framboisiers
    - planter groseilliers
    - entretien cassissiers
    - création jardin fruitier
    - petits fruits maison
    - récolte petits fruits
    - culture framboisiers jardin
    - conseils jardinage fruits
    - plantation arbustes fruitiers
image: /img/img-cover-crer-jardin-petits-fruits.webp
summary: >-
  Aménager un jardin de petits fruits est une excellente façon de profiter de
  fruits frais, juteux et savoureux à la maison. Les framboisiers, groseilliers
  et cassissiers sont des arbustes fruitiers qui s’intègrent parfaitement dans
  un jardin, qu’il soit grand ou de taille plus modeste. Ces fruits sont riches
  en vitamines, faciles à cultiver et ne nécessitent pas beaucoup d'entretien si
  vous respectez quelques principes de base. Dans cet article, nous vous
  guiderons à travers les étapes pour créer un jardin de petits fruits en
  abordant les besoins spécifiques de chaque plante, de la plantation à
  l’entretien.


  Pourquoi créer un jardin de petits fruits ?


  Un jardin dédié aux petits fruits présente plusieurs avantages, tant sur le plan pratique qu’esthétique. Voici quelques raisons pour lesquelles la création d’un tel jardin est un excellent projet
slug: comment-cr-er-un-jardin-de-petits-fruits-conseils-pour-planter-framboisiers-groseilliers-et-cassissiers
lastmod: 2024-11-09T17:31:02.644Z
category: fruitiers/petits-fruits/_index
published: 2024-11-09T17:12:00.000Z
weight: 0
administrable: true
url: fiches-conseils/fruitiers/petits-fruits/comment-cr-er-un-jardin-de-petits-fruits-conseils-pour-planter-framboisiers-groseilliers-et-cassissiers
kind: page
---
Aménager un jardin de petits fruits est une excellente façon de profiter de fruits frais, juteux et savoureux à la maison. Les framboisiers, groseilliers et cassissiers sont des arbustes fruitiers qui s’intègrent parfaitement dans un jardin, qu’il soit grand ou de taille plus modeste. Ces fruits sont riches en vitamines, faciles à cultiver et ne nécessitent pas beaucoup d'entretien si vous respectez quelques principes de base. Dans cet article, nous vous guiderons à travers les étapes pour créer un jardin de petits fruits en abordant les besoins spécifiques de chaque plante, de la plantation à l’entretien.

![Pourquoi créer un jardin de petits fruits ?](/img/img-articles-creer-jardin-petits-fruits.webp "Pourquoi créer un jardin de petits fruits ?")

## 1. Pourquoi créer un jardin de petits fruits ?

Un jardin dédié aux petits fruits présente plusieurs avantages, tant sur le plan pratique qu’esthétique. Voici quelques raisons pour lesquelles la création d’un tel jardin est un excellent projet :

### 1.1 Récolte abondante de fruits frais

Planter des framboisiers, des groseilliers et des cassissiers permet de récolter régulièrement des fruits frais à déguster tout au long de la saison. Ces petits fruits sont parfaits pour la consommation fraîche, les confitures, les jus ou les pâtisseries.

### 1.2 Facilité de culture et d’entretien

Ces arbustes fruitiers sont relativement faciles à cultiver et ne nécessitent pas d’entretien complexe. Ils s’adaptent bien à différentes conditions de sol et de climat, ce qui en fait des plantes idéales pour les jardiniers débutants ou ceux qui recherchent une culture peu exigeante.

### 1.3 Gain d’espace et adaptation aux petits jardins

Même si vous disposez d’un petit jardin, ces petits fruitiers peuvent être cultivés dans des espaces restreints, comme des bordures, des carrés potagers ou des pots. De plus, ils ajoutent un attrait visuel grâce à leur feuillage décoratif et leurs fruits colorés.

![Choisir le bon emplacement pour les petits fruits](/img/img-articles-bon-emplacement-jardin-petits-fruits.webp "Choisir le bon emplacement pour les petits fruits")

## 2. Choisir le bon emplacement pour les petits fruits

Le choix de l'emplacement est crucial pour assurer une croissance optimale de vos framboisiers, groseilliers et cassissiers. Voici quelques éléments à prendre en compte pour bien choisir l'emplacement de votre jardin de petits fruits.

### 2.1 Exposition au soleil

La plupart des petits fruits, comme les framboisiers et les groseilliers, ont besoin d'une exposition ensoleillée pour produire une récolte abondante. Il est donc important de choisir un emplacement qui reçoit au moins 6 heures de soleil par jour. Cependant, les cassissiers tolèrent mieux les zones légèrement ombragées, ce qui en fait un excellent choix pour les endroits partiellement ombragés de votre jardin.

### 2.2 Type de sol

Un sol bien drainé est essentiel pour la réussite de votre jardin de petits fruits. Les framboisiers, groseilliers et cassissiers préfèrent un sol riche en matière organique, légèrement acide (pH entre 5,5 et 6,5). Avant de planter, vous pouvez amender le sol avec du compost ou du fumier bien décomposé pour enrichir la terre en nutriments.

* **Sol trop argileux ou compact :** Si votre sol est trop lourd, ajoutez du sable ou du compost pour améliorer le drainage.
* **Sol trop sablonneux :** Dans ce cas, mélangez du compost pour retenir l'humidité et apporter les nutriments nécessaires.

### 2.3 Espacement des plants

Les petits fruitiers ont besoin d'espace pour bien se développer. Voici les distances de plantation à respecter pour chaque type d’arbuste :

* **Framboisiers :** Espacez les plants d’environ 50 cm à 1 mètre, avec 1,5 à 2 mètres entre les rangées.
* **Groseilliers :** Espacez les plants de 1 à 1,2 mètre.
* **Cassissiers :** Espacez les plants de 1,2 à 1,5 mètre.

![Planter les framboisiers, groseilliers et cassissiers](/img/img-articles-creer-jardin-petits-fruits-framboisier.webp "Planter les framboisiers, groseilliers et cassissiers")

## 3. Planter les framboisiers, groseilliers et cassissiers

Une fois l'emplacement choisi et préparé, il est temps de planter vos petits fruitiers. Voici les étapes spécifiques pour planter chaque type de fruitier.

### 3.1 Planter les framboisiers

Les framboisiers se plantent généralement à l'automne ou au début du printemps, lorsque le sol est encore frais.

* **Préparation du sol :** Avant de planter, ameublissez bien le sol et ajoutez du compost ou du fumier bien décomposé pour enrichir le sol.
* **Plantation :** Creusez un trou suffisamment large pour accueillir la motte du plant. Placez les racines dans le trou, puis recouvrez avec la terre en veillant à ne pas enterrer trop profondément la tige. Tassez légèrement la terre autour de la plante et arrosez généreusement.
* **Soutien des plants :** Les framboisiers étant des plantes à tiges souples, il est recommandé de les soutenir avec des tuteurs ou un treillis pour les aider à se maintenir debout.

### 3.2 Planter les groseilliers

Les groseilliers peuvent être plantés à l’automne ou au printemps, en fonction des conditions climatiques.

* **Préparation du sol :** Comme pour les framboisiers, le sol doit être bien ameubli et enrichi avec du compost. Assurez-vous que le sol est bien drainé pour éviter que les racines ne pourrissent.
* **Plantation :** Creusez un trou plus large que la motte, puis placez la plante de manière à ce que la base des tiges soit juste au-dessus du sol. Comblez avec la terre et tassez légèrement avant d’arroser abondamment.
* **Espacement :** Respectez une distance d'environ 1 mètre entre chaque plant pour permettre aux groseilliers de bien se développer.

### 3.3 Planter les cassissiers

Les cassissiers sont également plantés à l’automne ou au début du printemps. Ils tolèrent une légère ombre, ce qui les rend plus flexibles en termes d’emplacement.

* **Préparation du sol :** Le cassissier préfère un sol bien drainé et légèrement acide. Si votre sol est trop calcaire, vous pouvez l’amender avec de la terre de bruyère pour réduire le pH.
* **Plantation :** Creusez un trou suffisamment large pour accueillir la motte. Placez le plant de manière à ce que la base des tiges soit à niveau avec le sol, puis remplissez avec la terre en tassant légèrement.
* **Espacement :** Les cassissiers ont besoin de plus d'espace, donc laissez environ 1,5 mètre entre chaque plant pour leur permettre de bien se développer.

![L’entretien des petits fruitiers](/img/img-articles-creer-jardin-petits-fruits-entretien.webp "L’entretien des petits fruitiers")

## 4. L’entretien des petits fruitiers

Une fois vos framboisiers, groseilliers et cassissiers plantés, il est essentiel de leur offrir un entretien régulier pour garantir une croissance saine et une récolte abondante.

### 4.1 Arrosage régulier

Les petits fruits nécessitent un arrosage régulier, surtout pendant les périodes de sécheresse. Cependant, il est important de ne pas laisser le sol devenir détrempé, car cela peut entraîner la pourriture des racines.

* **Framboisiers :** Arrosez régulièrement, en particulier pendant la période de fructification. Le sol doit rester humide mais bien drainé.
* **Groseilliers et cassissiers :** Arrosez modérément, en maintenant un sol frais sans excès d'humidité.

### 4.2 Paillage

Le paillage est une excellente technique pour maintenir l'humidité du sol et prévenir la croissance des mauvaises herbes autour de vos plants.

* **Matériaux de paillage :** Vous pouvez utiliser de la paille, des copeaux de bois ou des feuilles mortes comme paillis. Étalez une couche de 5 à 7 cm autour des plantes pour conserver l'humidité et améliorer la structure du sol.

### 4.3 Fertilisation

Les petits fruitiers bénéficient d'un apport en nutriments supplémentaire, surtout au printemps, pour soutenir leur croissance et la production de fruits.

* **Engrais :** Utilisez un engrais riche en azote et en potassium, spécialement conçu pour les arbustes fruitiers. Appliquez-le au printemps pour encourager la croissance et une deuxième fois en été pour soutenir la production de fruits.

### 4.4 Taille des framboisiers, groseilliers et cassissiers

La taille est une étape essentielle pour favoriser la production de fruits et maintenir vos arbustes en bonne santé.

* **Framboisiers :** Taillez les framboisiers remontants (qui produisent des fruits deux fois par an) en supprimant les tiges mortes après la récolte d’automne. Pour les framboisiers non remontants, taillez en hiver, après la récolte estivale.
* **Groseilliers et cassissiers :** Taillez ces arbustes en hiver ou au début du printemps en éliminant les branches mortes, faibles ou trop anciennes. Cela stimule la nouvelle croissance et la fructification pour l’année suivante.

### 4.5 Protection contre les maladies et parasites

Les petits fruits peuvent être sensibles aux maladies et aux parasites, comme les pucerons, l’oïdium ou les cochenilles. Voici quelques astuces pour les protéger :

* **Surveillance :** Inspectez régulièrement les feuilles et les tiges pour détecter les premiers signes de maladie ou d’infestation de parasites.
* **Traitements naturels :** Utilisez du savon noir ou de l’huile de neem pour traiter les infestations légères. Pour prévenir l’oïdium, veillez à ce que vos plantes bénéficient d’une bonne aération et évitez d’arroser le feuillage.

![Récolter et profiter des petits fruits](/img/img-articles-creer-jardin-petits-fruits-recolter.webp "Récolter et profiter des petits fruits")

## 5. Récolter et profiter des petits fruits

Après avoir pris soin de vos petits fruitiers tout au long de l’année, il est enfin temps de récolter les fruits de votre travail.

### 5.1 Quand récolter ?

Les petits fruits sont généralement prêts à être récoltés en été, bien que les périodes exactes varient selon les variétés et les conditions climatiques.

* **Framboisiers :** Les framboises se récoltent lorsque les fruits sont bien colorés et se détachent facilement de la tige, généralement entre juin et septembre, selon la variété.
* **Groseilliers :** Les groseilles rouges et blanches sont prêtes à être cueillies dès que les fruits sont bien colorés, souvent entre juin et août.
* **Cassissiers :** Les cassis se récoltent en été, une fois que les baies sont bien noires et légèrement molles au toucher, généralement en juillet-août.

### 5.2 Comment récolter ?

Pour récolter vos petits fruits, il est préférable d'utiliser des ciseaux ou des sécateurs pour ne pas abîmer les branches.

* **Framboisiers :** Récoltez délicatement les framboises en les pinçant entre le pouce et l’index. Les fruits doivent se détacher facilement.
* **Groseilliers et cassissiers :** Coupez les grappes entières avec un sécateur, puis détachez les baies à la main.

## Conclusion

Créer un jardin de petits fruits avec des framboisiers, des groseilliers et des cassissiers est une expérience gratifiante qui vous permet de profiter de fruits frais et délicieux tout en embellissant votre jardin. En choisissant le bon emplacement, en respectant les besoins spécifiques de chaque plante et en offrant un entretien régulier, vous pourrez récolter des fruits juteux et savoureux chaque année. Avec un peu de patience et de soin, votre jardin de petits fruits deviendra rapidement une source de satisfaction et de délices pour toute la famille.
