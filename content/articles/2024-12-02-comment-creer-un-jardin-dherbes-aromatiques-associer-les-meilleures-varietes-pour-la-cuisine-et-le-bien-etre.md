---
title: "Comment créer un jardin d'herbes aromatiques : Associer les meilleures
  variétés pour la cuisine et le bien-être"
meta:
  keywords:
    - cultiver herbes aromatiques
    - jardinage maison
    - herbes fraîches
    - culture en pot
    - herbes pour la cuisine
    - jardin d’herbes aromatiques
    - basilic
    - menthe
    - persil
    - plantes aromatiques balcon
    - jardin naturel
    - récolte herbes
    - astuces jardinage
    - jardin urbain
    - nature et bien-être.
  description: Découvrez comment cultiver vos propres herbes aromatiques pour
    rester connecté à la nature et améliorer votre quotidien. Conseils pratiques
    pour une récolte fraîche et savoureuse, idéale pour sublimer vos plats.
image: /img/img-cover-ssocier-meilleures-varietes.webp
summary: >-
  Créer un jardin d'herbes aromatiques est une excellente manière d’apporter des
  saveurs fraîches à votre cuisine tout en bénéficiant des bienfaits pour la
  santé et le bien-être que ces plantes peuvent offrir. Que vous disposiez d’un
  grand jardin, d’un petit balcon ou d’une terrasse, il est possible de cultiver
  une grande variété d’herbes aromatiques en utilisant des techniques simples et
  efficaces. Dans cet article, nous allons vous montrer comment associer
  différentes plantes aromatiques dans un jardin dédié, ainsi que des conseils
  pratiques pour maximiser la récolte, de la plantation au soin des plantes.


  Un jardin d’herbes aromatiques offre de nombreux avantages, que ce soit pour la cuisine ou pour améliorer votre bien-être général.
slug: comment-cr-er-un-jardin-d-herbes-aromatiques-associer-les-meilleures-vari-t-s-pour-la-cuisine-et-le-bien-tre
lastmod: 2024-12-02T18:51:29.025Z
category: graines-de-fleurs-et-bulbes/graines-aromatiques/_index
published: 2024-12-02T18:39:00.000Z
weight: 0
administrable: true
url: fiches-conseils/graines-de-fleurs-et-bulbes/graines-aromatiques/comment-cr-er-un-jardin-d-herbes-aromatiques-associer-les-meilleures-vari-t-s-pour-la-cuisine-et-le-bien-tre
kind: page
---
Créer un jardin d'herbes aromatiques est une excellente manière d’apporter des saveurs fraîches à votre cuisine tout en bénéficiant des bienfaits pour la santé et le bien-être que ces plantes peuvent offrir. Que vous disposiez d’un grand jardin, d’un petit balcon ou d’une terrasse, il est possible de cultiver une grande variété d’herbes aromatiques en utilisant des techniques simples et efficaces. Dans cet article, nous allons vous montrer comment associer différentes plantes aromatiques dans un jardin dédié, ainsi que des conseils pratiques pour maximiser la récolte, de la plantation au soin des plantes.

![Pourquoi créer un jardin d’herbes aromatiques ?](/img/img-article-associer-meilleures-varietes-pourquoi.webp "Pourquoi créer un jardin d’herbes aromatiques ?")

## 1. Pourquoi créer un jardin d’herbes aromatiques ?

Un jardin d’herbes aromatiques offre de nombreux avantages, que ce soit pour la cuisine ou pour améliorer votre bien-être général.

### 1.1 Saveurs fraîches pour la cuisine

Cultiver vos propres herbes vous permet d’avoir des saveurs fraîches et naturelles toujours à portée de main. Cela apporte une nouvelle dimension à vos plats, que ce soit des salades, des sauces, des marinades ou des thés.

### 1.2 Des bienfaits pour la santé

Les herbes aromatiques sont souvent riches en antioxydants, vitamines et minéraux. Certaines ont des propriétés médicinales reconnues, comme la menthe pour faciliter la digestion ou le thym pour ses vertus antiseptiques.

### 1.3 Une approche écologique

Cultiver vos propres herbes aromatiques est également une démarche écologique. Vous réduisez votre consommation de produits emballés, minimisez votre empreinte carbone et avez la possibilité de cultiver sans utiliser de pesticides chimiques.

![Planifier son jardin d’herbes aromatiques](/img/img-article-associer-meilleures-varietes-planifier.webp "Planifier son jardin d’herbes aromatiques")

## 2. Planifier son jardin d’herbes aromatiques

Avant de commencer, il est important de bien planifier votre jardin d’herbes aromatiques en tenant compte de vos besoins, de l’espace disponible, et des conditions de culture.

### 2.1 Choisir l’emplacement idéal

Les herbes aromatiques ont besoin de lumière pour bien pousser. La plupart d’entre elles préfèrent un ensoleillement direct pendant au moins 6 heures par jour. Toutefois, certaines variétés tolèrent mieux l’ombre partielle.

* **Plein soleil :** Le basilic, le thym, le romarin et l’origan prospèrent sous un ensoleillement intense.
* **Mi-ombre :** La menthe, la coriandre et la ciboulette préfèrent des endroits plus ombragés, surtout dans les régions chaudes.

#### 2.2 Choisir des contenants ou une parcelle de jardin

Selon l’espace dont vous disposez, vous pouvez choisir de cultiver vos herbes dans des pots, des jardinières ou directement en pleine terre. Si vous manquez de place, les pots sont une solution idéale et flexible, car vous pouvez les déplacer selon les besoins de lumière et les saisons.

* **Pots et jardinières :** Assurez-vous qu'ils sont bien drainés pour éviter que l'eau ne stagne.
* **Pleine terre :** Si vous avez un jardin, créez des massifs dédiés aux herbes aromatiques ou intégrez-les dans vos parterres de fleurs.

![Associer les meilleures variétés d’herbes aromatiques](/img/img-article-associer-meilleures-varietes-associer.webp "Associer les meilleures variétés d’herbes aromatiques")

## 3. Associer les meilleures variétés d’herbes aromatiques

Il est important de bien associer les herbes dans votre jardin pour optimiser l’espace, encourager une croissance saine et éviter les incompatibilités.

### 3.1 Les herbes qui aiment le soleil

Certaines herbes méditerranéennes préfèrent les sols secs et bien drainés, avec un ensoleillement maximum. En les plantant ensemble, vous simplifiez l’entretien, car elles partagent les mêmes besoins en eau et en lumière.

#### Exemples d’herbes à associer :

* **Thym (Thymus vulgaris) :** Excellente pour les plats méditerranéens et les infusions, le thym résiste bien à la sécheresse et préfère un sol pauvre.
* **Romarin (Rosmarinus officinalis)** : Le romarin est un arbuste vivace qui prospère en plein soleil et ajoute une touche parfumée aux rôtis et marinades.
* **Origan (Origanum vulgare) :** Parfait pour les sauces et les pizzas, l’origan aime les conditions sèches et ensoleillées.
* **Basilic (Ocimum basilicum) :** Bien qu'il nécessite un arrosage plus régulier, le basilic prospère également sous le soleil.

### 3.2 Les herbes qui préfèrent l’ombre

D’autres herbes aromatiques préfèrent des conditions plus fraîches et tolèrent mieux l’ombre partielle. Ces plantes ont généralement besoin d’un arrosage plus régulier.

#### Exemples d’herbes à associer :

* **Menthe (Mentha spp.) :** La menthe est très facile à cultiver, mais peut devenir envahissante, c’est pourquoi elle est souvent cultivée en pot. Elle aime les sols humides et une exposition à mi-ombre.
* **Coriandre (Coriandrum sativum) :** La coriandre monte rapidement en graines sous un ensoleillement trop intense. Elle préfère une lumière tamisée et des arrosages réguliers.
* **Ciboulette (Allium schoenoprasum) :** Parfaite pour les omelettes et les salades, la ciboulette pousse bien à l’ombre et produit des fleurs comestibles décoratives.
* **Persil (Petroselinum crispum) :** Le persil est une herbe polyvalente qui pousse bien à l’ombre légère, surtout durant les mois les plus chauds.

![Comment bien planter et entretenir son jardin d'herbes aromatiques](/img/img-article-associer-meilleures-varietes-entretenir.webp "Comment bien planter et entretenir son jardin d'herbes aromatiques")

## 4. Comment bien planter et entretenir son jardin d'herbes aromatiques

Une fois que vous avez choisi vos variétés et planifié votre jardin, voici quelques étapes essentielles pour assurer une plantation réussie et un entretien efficace.

### 4.1 Planter les graines ou plants d’herbes aromatiques

#### Étapes pour semer des graines d’herbes aromatiques :

1. **Préparation du sol :** Assurez-vous que le sol est bien drainé et enrichi de compost ou de terreau pour faciliter la germination.
2. **Semis :** Semez les graines à la surface du sol ou à une profondeur de 1 à 2 cm selon les variétés. Maintenez une distance suffisante entre les plants pour éviter qu'ils ne se concurrencent.
3. **Arrosage :** Arrosez légèrement après la plantation pour garder le sol humide sans excès d'eau.

##### Utilisation de jeunes plants :

Si vous choisissez de planter des herbes déjà développées, faites attention à bien les espacer et à leur offrir suffisamment d’espace pour se développer.

### 4.2 Arrosage des herbes aromatiques

L’arrosage est une étape cruciale pour la bonne croissance des herbes aromatiques. Voici quelques conseils pour adapter l’arrosage à chaque type d'herbe :

* **Herbes méditerranéennes :** Le romarin, le thym et l’origan préfèrent les sols secs. Laissez le sol sécher légèrement entre deux arrosages pour éviter de trop les arroser.
* **Herbes à feuilles tendres :** Le basilic, la coriandre et la menthe ont besoin d’un arrosage plus régulier pour éviter que leurs feuilles ne se dessèchent. Arrosez le matin pour éviter les maladies fongiques.

### 4.3 Paillage et entretien

Le paillage est une méthode efficace pour retenir l’humidité et éviter la prolifération des mauvaises herbes. Utilisez un paillis organique comme des copeaux de bois ou des feuilles mortes pour couvrir le sol autour de vos herbes.

* **Taille régulière :** Taillez régulièrement vos herbes pour encourager une nouvelle croissance et éviter qu'elles ne montent en graines trop rapidement. Par exemple, pincez les feuilles de basilic pour favoriser une croissance buissonnante.

### 4.4 Utilisation d’engrais naturels

L’utilisation d’engrais naturels permet de stimuler la croissance de vos plantes tout en évitant les produits chimiques. Appliquez du compost ou un engrais liquide à base de plantes toutes les 4 à 6 semaines pendant la saison de croissance.

![Maximiser la récolte de vos herbes aromatiques](/img/img-article-associer-meilleures-varietes-maximiser.webp "Maximiser la récolte de vos herbes aromatiques")

## 5. Maximiser la récolte de vos herbes aromatiques

Pour obtenir une récolte abondante et continue de vos herbes aromatiques, il est important de savoir quand et comment les récolter.

### 5.1 Récolte régulière

La récolte régulière des feuilles stimule la production de nouvelles pousses. Vous pouvez commencer à cueillir les feuilles d’herbes comme le basilic ou la menthe dès que les plantes ont plusieurs ensembles de feuilles bien développées.

* **Récolter le matin :** Les huiles essentielles sont plus concentrées dans les herbes le matin. C’est donc le meilleur moment pour récolter les feuilles si vous souhaitez les utiliser fraîches ou les conserver.
* **Éviter les coupes radicales :** Ne retirez jamais plus d’un tiers de la plante à la fois pour ne pas la stresser.

### 5.2 Séchage et conservation des herbes aromatiques

Si vous avez une production abondante, vous pouvez sécher vos herbes pour les conserver tout au long de l’année. Voici quelques méthodes pour conserver vos herbes aromatiques :

* **Séchage à l’air libre :** Attachez vos herbes en petits bouquets et suspendez-les dans un endroit sec et sombre pour qu’elles sèchent doucement.
* **Congélation :** Vous pouvez également congeler les feuilles d’herbes comme la menthe, le basilic ou la coriandre dans des bacs à glaçons avec un peu d’eau ou d’huile pour les utiliser plus tard.

### 5.3 Utiliser les fleurs des herbes aromatiques

Certaines herbes, comme la ciboulette, produisent des fleurs comestibles. Ces fleurs peuvent être utilisées en cuisine pour ajouter une touche de couleur et de saveur aux plats.

* **Fleurs de ciboulette :** Parfaites dans les salades ou comme garniture.
* **Fleurs de thym :** Délicates et légèrement sucrées, elles sont excellentes pour parfumer les marinades ou les infusions.

## Conclusion

Créer un jardin d’herbes aromatiques est une activité à la fois pratique et agréable qui vous permet de disposer de plantes fraîches et savoureuses toute l'année. En planifiant soigneusement votre espace, en choisissant les bonnes associations de plantes et en suivant des pratiques de culture simples, vous pouvez maximiser votre récolte et profiter des bienfaits pour la cuisine et le bien-être. Que vous disposiez d’un grand jardin ou d’un petit balcon, cultiver vos propres herbes aromatiques est un excellent moyen de rester connecté à la nature et d'améliorer votre quotidien.
