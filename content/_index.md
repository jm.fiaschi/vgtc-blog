---
meta:
  keywords:
    - végétal
    - plante
    - plantes
    - jardinage
    - botanique
    - fleurs
    - arbres
    - jardins
    - paysage
    - nature
    - plantes d'intérieur
    - test
  description: La première plateforme réunissant tous les passionés du végétal !
image: /img/c2911c3580ccfb3488a132d89d76367d.webp
limit: 4
lastmod: 2024-11-05T08:44:10.225Z
title: Accueil
slug: accueil
---
# Tout l'univers **végétal** réuni sur une plateforme.

Très prochainement, réservez des ateliers créatifs, profitez de ventes 
flash grâce à nos fleuristes partenaires. Achetez, échangez des 
plantes, via notre marketplace ou confiez vos plantes à nos 
plant-sisters passionnées lors de vos absences. Vous pouvez dès 
à présent consulter nos fiches conseils et l’agenda des événements 
près de chez vous.
