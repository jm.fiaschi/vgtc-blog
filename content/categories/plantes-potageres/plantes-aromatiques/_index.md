---
meta:
  keywords:
    - Plantes aromatiques
  description: Plantes aromatiques
image: /img/default-category-background.webp
summary: Plantes aromatiques
weight: 0
slug: plantes-potageres/plantes-aromatiques/_index
lastmod: 2024-12-06T11:40:47.929Z
title: Plantes aromatiques
published: 2024-12-06T11:39:00.000Z
kind: section
administrable: true
---
