---
title: Arbre, Arbuste, fruitier, petit fruit
meta:
  keywords:
  - arbre
  - arbuste
  - fruitier
  - petit fruit
published: 2024-08-21T10:07:00.000Z
summary: Arbre, Arbuste, fruitier, petit fruit
slug: arbre-arbuste-fruitier-petit-fruit/_index
lastmod: 2024-09-05T20:22:48.872Z
kind: section
administrable: true
---
