import os
from PIL import Image
import re

# Folder paths
image_folder = "static/img"
content_folder = "content"
layouts_folder = "layouts"

# Supported image extensions for conversion
supported_extensions = ['.jpg', '.jpeg', '.png']

def convert_image_to_webp(image_path, quality=80):
    """Convert an image to WebP format with the specified compression quality and save it."""
    file_name, ext = os.path.splitext(image_path)
    
    # Only process supported image types
    if ext.lower() in supported_extensions:
        webp_path = file_name + ".webp"
        img = Image.open(image_path)
        
        # Save the image as WebP with 80% quality compression
        img.save(webp_path, "webp", quality=quality)
        print(f"Converted {image_path} to {webp_path} with {quality}% compression")
        return webp_path
    return None

def convert_images_in_folder(folder, quality=80):
    """Convert all supported images in a folder to WebP format with compression."""
    converted_images = {}
    
    for root, _, files in os.walk(folder):
        for file in files:
            file_path = os.path.join(root, file)
            converted = convert_image_to_webp(file_path, quality)
            if converted:
                # Save the old file name and new webp file for later replacement in articles
                converted_images[file] = os.path.basename(converted)
    
    return converted_images

def update_image_paths_in_files(folder, converted_images, file_extensions):
    """Update files in a folder (recursively) to point to the new WebP images."""
    md_pattern = r'!\[.*?\]\((.*?)\)'  # Markdown image pattern ![alt](path)
    html_pattern = r'<img\s+[^>]*src=["\'](.*?)["\']'  # HTML img src pattern

    # Regular expression pattern for images based on file type
    patterns = {
        '.md': md_pattern,
        '.html': html_pattern,
    }

    for root, _, files in os.walk(folder):
        for file in files:
            file_ext = os.path.splitext(file)[1]
            if file_ext in file_extensions:
                file_path = os.path.join(root, file)
                
                # Read the content of the file
                with open(file_path, 'r', encoding='utf-8') as content_file:
                    content = content_file.read()

                # Replace image paths with .webp versions
                new_content = content
                for old_image, new_image in converted_images.items():
                    new_content = re.sub(rf'({old_image})', new_image, new_content)

                # If changes were made, write the updated content back
                if new_content != content:
                    with open(file_path, 'w', encoding='utf-8') as content_file:
                        content_file.write(new_content)
                    print(f"Updated {file_path} with WebP images.")

def delete_original_images(folder, converted_images):
    """Delete original images that have been converted to WebP."""
    for root, _, files in os.walk(folder):
        for file in files:
            if file in converted_images:
                # Check if the file is not a webp and delete it
                file_path = os.path.join(root, file)
                if os.path.splitext(file_path)[1].lower() in supported_extensions:
                    os.remove(file_path)
                    print(f"Deleted original image: {file_path}")

def main():
    # Step 1: Convert all images to WebP in the static/img folder with 80% compression
    converted_images = convert_images_in_folder(image_folder, quality=80)

    # Step 2: Update .md files in content folder and .html files in layouts folder
    update_image_paths_in_files(content_folder, converted_images, ['.md'])
    update_image_paths_in_files(layouts_folder, converted_images, ['.html'])

    # Step 3: Delete the original images after conversion
    delete_original_images(image_folder, converted_images)

if __name__ == "__main__":
    main()
