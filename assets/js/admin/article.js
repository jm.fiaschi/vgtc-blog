var preview = createClass({
    render: function () {
        const entry = this.props.entry;
        const image = entry.getIn(['data', 'image']);
        const bg = this.props.getAsset(image);
        return h('body', {},
            h('div', { "id": "background", style: { 'background-image': 'url("' + bg.toString() + '")' } }),
            h('header', {},
                h('div', { "id": "navigation" }),
                h('div', { "id": "menu" }),
            ),
            h('main', {},
                h('article', {},
                    h('header', {},
                        h('div', { "className": "inner" },
                            h('p', { "className": "category" }, entry.getIn(['data', 'category'])),
                            h('h1', {}, entry.getIn(['data', 'title'])),
                        )
                    ),
                    h('div', { "id": "breadcrumbs" },
                        h('a', { "href": "/", "title": "home" }, "Accueil")
                    ),
                    h('div', { "className": "body" },
                        h('section', {},
                            this.props.widgetFor('body')
                        )
                    )
                )
            ),
            h('link', { "rel": "stylesheet", "href": fontCssPath }),
            h('link', { "rel": "stylesheet", "href": layoutCssPath }),
            h('link', { "rel": "stylesheet", "href": articleCssPath }),
        );
    }
});
CMS.registerPreviewTemplate('articles', preview);