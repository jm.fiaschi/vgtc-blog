var preview = createClass({
    render: function () {
        const entry = this.props.entry;
        const image = entry.getIn(['data', 'image']);
        const bg = this.props.getAsset(image);
        return h('body', {},
            h('div', { "id": "background", style: { 'background-image': 'url("' + bg.toString() + '")' } }),
            h('header', {},
                h('div', { "id": "navigation" }),
            ),
            h('main', {},
                h('div', { "id": "breadcrumbs" },
                    h('a', { "href": "/", "title": "home" }, "Accueil")
                ),
                h('article', {},
                    h('header', {},
                        h('div', { "className": "inner" },
                            h('p', { "className": "category" }, entry.getIn(['data', 'parent'])),
                            h('h1', {}, entry.getIn(['data', 'title'])),
                        )
                    ),
                    this.props.widgetFor('body')
                )
            ),
            h('link', { "rel": "stylesheet", "href": fontCssPath }),
            h('link', { "rel": "stylesheet", "href": layoutCssPath }),
            h('link', { "rel": "stylesheet", "href": articleCssPath }),
        );
    }
});
CMS.registerPreviewTemplate('qui-sommes-nous', preview);
