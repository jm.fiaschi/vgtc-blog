// Fonction pour obtenir le nom du mois en français
function getFrenchMonthName(monthIndex) {
    const months = [
        'Jan', 'Fév', 'Mar', 'Avr', 'Mai', 'Jun',
        'Jul', 'Aoû', 'Sep', 'Oct', 'Nov', 'Déc'
    ];
    return months[monthIndex];
}

var preview = createClass({
    render: function () {
        const entry = this.props.entry;
        const published = new Date(entry.getIn(['data', 'published']));
        let formattedPublishedDate = `${published.getDate()} ${getFrenchMonthName(published.getMonth())} ${published.getFullYear()} à ${published.getHours()}:${('0' + published.getMinutes()).slice(-2)}`;
        const updated = new Date(entry.getIn(['data', 'lastmod']));
        let formattedUpdatedDate = `${updated.getDate()} ${getFrenchMonthName(updated.getMonth())} ${updated.getFullYear()} à ${updated.getHours()}:${('0' + updated.getMinutes()).slice(-2)}`;
        return h('body', {},
            h('header', {},
                h('div', { "id": "navigation" }),
            ),
            h('main', {},
                h('article', {},
                    h('header', {},
                        h('div', { "id": "breadcrumbs" },
                            h('a', { "href": "/", "title": "home" }, "Accueil"),
                            h('span', {}, "/"),
                            h('a', { "href": "/agenda-des-%C3%A9v%C3%A9nements/", "title": "Agenda des événements" }, "Agenda des événements"),
                            h('span', {}, "/"),
                            entry.getIn(['data', 'title']),
                        ),
                        h('div', { "className": "inner" },
                            h('h1', {}, entry.getIn(['data', 'title'])),
                            h('span', {},
                                "Publié le " + formattedPublishedDate + " par la rédaction de Vegetaclick (Modifié le " + formattedUpdatedDate + ")"
                            ),
                        )
                    ),
                    this.props.widgetFor('body')
                )
            ),
            h('link', { "rel": "stylesheet", "href": fontCssPath }),
            h('link', { "rel": "stylesheet", "href": layoutCssPath }),
            h('link', { "rel": "stylesheet", "href": agendaCssPath }),
            h('link', { "rel": "stylesheet", "href": eventCssPath }),
        );
    }
});
CMS.registerPreviewTemplate('events', preview);