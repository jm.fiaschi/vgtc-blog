function filter_init(elt) {
    const url = window.location.href;
    const urlObj = new URL(url);
    const params = new URLSearchParams(urlObj.search);
    const paramName = elt.id;

    elt.value = params.get(paramName);
}

function filter() {
    const articles = document.querySelectorAll("#articles article");
    const filters = document.querySelectorAll("#filter select");

    articles.forEach(function (article, index) {
        for (const filter of filters) {
            if (filter.value !== 'null' && article.querySelector('input[name="' + filter.id + '"]').value !== filter.value) {

                article.classList.add('hidden');
                break;
            }
            article.classList.remove('hidden');
        }
    });
}