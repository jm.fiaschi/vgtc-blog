function progress() {
    const index = document.getElementById("progress-bar-index");
    const total = document.getElementById("progress-bar-total");

    if (!index & !total) {
        return;
    }

    const indexValue = index.innerHTML;
    const totalValue = total.innerHTML;

    var scrolled = (indexValue / totalValue) * 100;
    document.getElementById("progress-bar").style.width = scrolled + "%";
}

function next(event, limit) {
    event.preventDefault();
    var index = document.getElementById("progress-bar-index");
    const total = document.getElementById("progress-bar-total");
    var articles = document.querySelectorAll("#articles article");

    if (!index & !total) {
        return;
    }

    var indexValue = index.innerHTML;
    const totalValue = total.innerHTML;

    var endIndex = parseInt(indexValue) + parseInt(limit);
    if (endIndex >= totalValue) {
        endIndex = totalValue
        document.getElementById("progress-bar-next").classList.add("disabled");
    }

    for (var i = parseInt(indexValue); i < endIndex; i++) {
        if (articles[i]) {
            articles[i].classList.remove('hidden');
        }
    }

    indexValue = i;
    document.getElementById("progress-bar-index").innerHTML = indexValue;
}

window.onload = function () {
    const index = document.getElementById("progress-bar-index");
    if (!index) {
        return;
    }

    progress();

    const observer = new MutationObserver(function (mutations) {
        mutations.forEach(function (mutation) {
            if (mutation.type == "childList") {
                progress();
            }
        });
    });
    observer.observe(document.getElementById("progress-bar-index"), { childList: true });
};
