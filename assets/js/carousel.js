const scrollPerClick = 215;

function goLeft() {
    const carousel = document.querySelector('.carousel .inner');
    const rightButton = document.querySelector('.carousel button.right');
    const leftButton = document.querySelector('.carousel button.left');
    const maxScroll = carousel.scrollWidth - carousel.clientWidth;

    if (carousel.scrollLeft < 0) {
        carousel.scrollLeft = 0;
    } else {
        carousel.scrollLeft -= scrollPerClick;
    }

    leftButton.disabled = carousel.scrollLeft === 0;
    rightButton.disabled = carousel.scrollLeft >= maxScroll;
}

function goRight() {
    const carousel = document.querySelector('.carousel .inner');
    const rightButton = document.querySelector('.carousel button.right');
    const leftButton = document.querySelector('.carousel button.left');
    const maxScroll = carousel.scrollWidth - carousel.clientWidth;

    if (carousel.scrollLeft + scrollPerClick > maxScroll) {
        carousel.scrollLeft = maxScroll;
        rightButton.disabled = true;
    } else {
        carousel.scrollLeft += scrollPerClick;
    }

    leftButton.disabled = carousel.scrollLeft === 0;
    rightButton.disabled = carousel.scrollLeft >= maxScroll;
}

function refresh() {
    const carousel = document.querySelector('.carousel .inner');
    const rightButton = document.querySelector('.carousel button.right');
    const leftButton = document.querySelector('.carousel button.left');
    const maxScroll = carousel.scrollWidth - carousel.clientWidth;

    leftButton.disabled = carousel.scrollLeft === 0;
    rightButton.disabled = carousel.scrollLeft >= maxScroll;
}