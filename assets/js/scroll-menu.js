let scrollInterval;
const scrollSpeed = 3; // Vitesse de défilement en pixels

function autoScroll(event) {
    const container = document.querySelector('nav .x-scroll');
    const containerRect = container.getBoundingClientRect();
    const mouseX = event.clientX - containerRect.left;

    if (mouseX < 50) {
        clearInterval(scrollInterval); // Arrête tout défilement actuel

        // Commence à faire défiler vers la gauche progressivement
        scrollInterval = setInterval(function () {
            container.scrollLeft -= scrollSpeed;
        }, 10); // Intervalle de temps en millisecondes (plus petit = plus rapide)
    } else if (mouseX > containerRect.width - 50) {
        clearInterval(scrollInterval); // Arrête tout défilement actuel

        // Commence à faire défiler vers la droite progressivement
        scrollInterval = setInterval(function () {
            container.scrollLeft += scrollSpeed;
        }, 10); // Intervalle de temps en millisecondes (plus petit = plus rapide)
    } else {
        clearInterval(scrollInterval); // Arrête tout défilement actuel
    }
}

function stopAutoScroll() {
    clearInterval(scrollInterval); // Arrête tout défilement lorsque la souris quitte la zone
}