function lazyLoadBackground() {
    const lazyBackgrounds = document.querySelectorAll('.lazy');

    const observer = new IntersectionObserver((entries, observer) => {
        entries.forEach(entry => {
            if (entry.isIntersecting) {
                const lazyBg = entry.target;
                const bgUrl = lazyBg.getAttribute('data-bg');

                // Appliquer l'image de fond
                lazyBg.style.backgroundImage = `url('${bgUrl}')`;

                // Désactiver l'observation une fois l'image chargée
                observer.unobserve(lazyBg);
            }
        });
    });

    lazyBackgrounds.forEach(lazyBg => {
        observer.observe(lazyBg);
    });
}
document.addEventListener("DOMContentLoaded", lazyLoadBackground);