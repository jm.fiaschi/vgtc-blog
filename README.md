## Knowledge
- [hugo](https://gohugo.io/documentation/): Generate the static content of the site.
- CICD: Gitlab CI
- [Just](https://github.com/casey/just): run commands in local

## Getting Started

### Install and run in local

1. Clone the project in local
2. Install [just](https://github.com/casey/just?tab=readme-ov-file#packages)

```sh
curl --proto '=https' --tlsv1.2 -sSf https://just.systems/install.sh | bash -s -- --to ~/bin
echo 'export PATH="$PATH:$HOME/bin"' >> ~/.bashrc
just --help
```

3. Install dependancies
```sh
just install
```

4. Start hugo and the proxy
```sh
just start
# or
just restart
```

5. Access the to [site](http://localhost:1313). Add `/admin` to access the to administration page.